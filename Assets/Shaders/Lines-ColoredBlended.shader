﻿// Взято из: http://gamedev.stackexchange.com/questions/96964/how-to-correctly-draw-a-line-in-unity

Shader "Custom/Lines Colored Blended" 
{
	SubShader 
	{ 
		Pass 
		{
			Blend SrcAlpha OneMinusSrcAlpha
			ZWrite Off
			Cull Off
			Fog { Mode Off }
			BindChannels 
			{
				Bind "vertex", vertex
				Bind "color", color 
			}
		} 
	} 
}