﻿using UnityEngine;

namespace nikita600Toolkit.ObjectPoolSystem
{
    [DisallowMultipleComponent]
    public sealed class SceneObjectPool : MonoBehaviour
    {
        #region Fields
        
        [SerializeField]
        Transform m_ParentForObjects;

        [SerializeField]
        InternalObjectPool m_ObjectPool;

        #endregion
        #region UnityMessages

        void Awake()
        {
            m_ObjectPool.PoolTransform = m_ParentForObjects;
        }

        #endregion
        #region Methods

        public void Push(PoolableSceneObject _object)
        {
            m_ObjectPool.Push(_object);
        }

        public PoolableSceneObject Pop()
        {
            return m_ObjectPool.Pop();
        }

        #endregion
        #region Nested

        [System.Serializable]
        internal class InternalObjectPool : ObjectPool<PoolableSceneObject>
        {
            #region Fields

            [SerializeField]
            PoolableSceneObject m_Prefab;

            #endregion
            #region Properties
            
            internal Transform PoolTransform { get; set; }

            #endregion
            #region Methods

            public override void Push(PoolableSceneObject _object)
            {
                base.Push(_object);

                _object.gameObject.SetActive(false);
            }

            public override PoolableSceneObject Pop()
            {
                var poolableObject = base.Pop();

                poolableObject.gameObject.SetActive(true);

                return poolableObject;
            }

            public override PoolableSceneObject CreateObject()
            {
                var poolableObject = Instantiate<PoolableSceneObject>(m_Prefab);

                poolableObject.transform.SetParent(PoolTransform);
                poolableObject.ParentPool = this;

                return poolableObject;
            }

            #endregion
        }

        #endregion
    }
}