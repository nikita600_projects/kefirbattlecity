﻿using System.Collections.Generic;

namespace nikita600Toolkit.ObjectPoolSystem
{
    [System.Serializable]
    public class ObjectPool<T> where T : new ()
    {
        #region Fields
        
        readonly Stack<T> m_Pool = new Stack<T>();

        int m_CreatedObjectsCount;

        #endregion
        #region Methods
        
        public virtual T Pop()
        {
            if (m_Pool.Count == 0)
            {
                m_CreatedObjectsCount++;
                return CreateObject();
            }
            
            return m_Pool.Pop();
        }

        public virtual void Push(T _object)
        {
            //if (!m_Pool.Contains(_object))
                m_Pool.Push(_object);
        }

        /// <summary>
        /// Вызывается только внутри метода Pop.
        /// </summary>
        /// <returns></returns>
        public virtual T CreateObject()
        {
            return new T();
        }

        #endregion
    }
}
