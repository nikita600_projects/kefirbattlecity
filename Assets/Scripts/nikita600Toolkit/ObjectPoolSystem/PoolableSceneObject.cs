﻿using UnityEngine;
using UnityEngine.Events;

namespace nikita600Toolkit.ObjectPoolSystem
{
    [DisallowMultipleComponent]
    public sealed class PoolableSceneObject : MonoBehaviour
    {
        #region Fields

        [SerializeField]
        UnityEvent m_OnActivateObject;

        [SerializeField]
        UnityEvent m_OnDeactivateObject;
        
        SceneObjectPool.InternalObjectPool m_ParentPool;

        #endregion
        #region Properties

        internal SceneObjectPool.InternalObjectPool ParentPool
        {
            set { m_ParentPool = value; }
        }

        #endregion
        #region UnityMessages

        void OnEnable()
        {
            m_OnActivateObject.Invoke();
        }

        void OnDisable()
        {
            m_OnDeactivateObject.Invoke();
        }

        #endregion
        #region Methods

        public void ReturnToPool()
        {
            if (m_ParentPool != null)
                m_ParentPool.Push(this);
        }

        #endregion
    }
}