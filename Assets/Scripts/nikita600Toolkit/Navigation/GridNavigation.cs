﻿using nikita600Toolkit.GridComponents;
using UnityEngine;

namespace nikita600Toolkit.Navigation
{
    [DisallowMultipleComponent]
    [RequireComponent(typeof(Grid))]
    public class GridNavigation : Navigation
    {
        #region Fields

        Grid m_Grid;

        #endregion
        #region Properties

        Grid Grid
        {
            get
            {
                if (m_Grid == null)
                    m_Grid = GetComponent<Grid>();

                return m_Grid;
            }
        }

        GridLayer[] Layers
        {
            get { return GetComponentsInChildren<GridLayer>(); }
        }

        #endregion

    }
}