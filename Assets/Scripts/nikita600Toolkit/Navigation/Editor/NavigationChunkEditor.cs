﻿using nikita600Toolkit.EditorTools;
using nikita600Toolkit.GridComponents;
using System;
using UnityEngine;
using UnityEditor;

namespace nikita600Toolkit.Navigation
{
    [CustomEditor(typeof(NavigationChunk))]
    public class NavigationChunkEditor : Editor
    {
        #region Fields

        SerializedProperty m_Script;

        SerializedProperty m_TilePosition;
        SerializedProperty m_IsWalkable;
        SerializedProperty m_Cost;
        SerializedProperty m_Waypoint;

        Grid m_Grid;
        NavigationChunk m_NavChunk;

        [NonSerialized]
        bool m_IsEditMode;
        [NonSerialized]
        bool m_IsTileSelected;
        [NonSerialized]
        Vector2Int m_SelectedTilePosition;

        Tool m_LastTool;
        ViewTool m_LastViewTool;
        
        #endregion
        #region Properties

        Grid Grid
        {
            get
            {
                if (m_Grid == null)
                    m_Grid = NavChunk.GetComponent<Grid>();

                return m_Grid;
            }
        }

        NavigationChunk NavChunk
        {
            get
            {
                if (m_NavChunk == null)
                    m_NavChunk = target as NavigationChunk;

                return m_NavChunk;
            }
        }

        bool IsEditMode
        {
            get { return m_IsEditMode; }
            set
            {
                m_IsEditMode = value;

                if (m_IsEditMode)
                {
                    m_LastTool = Tools.current;
                    m_LastViewTool = Tools.viewTool;

                    Tools.current = Tool.View;
                    Tools.viewTool = ViewTool.FPS;
                }
                else
                {
                    Tools.current = m_LastTool;
                    Tools.viewTool = m_LastViewTool;

                    IsTileSelected = false;
                }

                SceneView.RepaintAll();
            }
        }

        bool IsTileSelected
        {
            get { return m_IsTileSelected; }
            set
            {
                m_IsTileSelected = value;

                if (!m_IsTileSelected)
                    return;

                GetSelectedTileProperties();
            }
        }

        #endregion
        #region Methods

        void OnEnable()
        {
            m_Script = CustomEditorUtility.GetScriptProperty(serializedObject);
        }
        
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            CustomEditorUtility.DrawDisabledField(m_Script);

            EditorGUILayout.BeginVertical(Style.BOX_FRAME);

            DrawEditButton();
            DrawSelectedTileData();

            if (IsEditMode && !IsTileSelected)
                EditorGUILayout.HelpBox("Select target 'NavigationTile' to edit data.", MessageType.Info);

            EditorGUILayout.EndVertical();
        }

        void OnSceneGUI()
        {
            SelectTileMode();
            DrawWaypoints();
            DrawWaypointHandles();
        }

        void GetSelectedTileProperties()
        {
            serializedObject.Update();

            var data = NavChunk.Data;
            var selectedTileIndex = data.FindIndex(chunkData => chunkData.Position == m_SelectedTilePosition);

            if (selectedTileIndex < 0)
            {
                var navTile = new NavigationTile();
                var navChunkData = new NavigationChunkData(m_SelectedTilePosition, navTile);
                data.Add(navChunkData);

                serializedObject.ApplyModifiedProperties();
                serializedObject.Update();

                selectedTileIndex = data.FindIndex(chunkData => chunkData.Position == m_SelectedTilePosition);
            }

            var navChunkDataProperty = serializedObject.FindProperty("m_Data");
            var chunkDataProp = navChunkDataProperty.GetArrayElementAtIndex(selectedTileIndex);
            var navTileData = chunkDataProp.FindPropertyRelative("m_TileData");

            m_TilePosition = chunkDataProp.FindPropertyRelative("m_Position");

            m_IsWalkable = navTileData.FindPropertyRelative("m_IsWalkable");
            m_Cost = navTileData.FindPropertyRelative("m_Cost");
            m_Waypoint = navTileData.FindPropertyRelative("m_Waypoint");

            Repaint();
        }

        #endregion
        #region InspectorDrawers

        void DrawEditButton()
        {
            var originalColor = GUI.color;

            if (IsEditMode)
                GUI.color = Color.green;

            var buttonLabel = IsEditMode ? Style.EXIT_EDIT_MODE_BUTTON_LABEL : Style.EDIT_MODE_BUTTON_LABEL;
            if (GUILayout.Button(buttonLabel))
                IsEditMode = !IsEditMode;

            GUI.color = originalColor;
        }

        void DrawSelectedTileData()
        {
            if (!IsTileSelected)
                return;
            
            EditorGUILayout.BeginVertical(Style.BOX_FRAME);

            GUILayout.Label(Style.SELECTED_NAV_TILE_DATA_LABEL, Style.BOLD_LABEL);

            serializedObject.Update();

            CustomEditorUtility.DrawDisabledField(m_TilePosition);
            EditorGUILayout.PropertyField(m_IsWalkable);
            EditorGUILayout.PropertyField(m_Cost);
            EditorGUILayout.PropertyField(m_Waypoint);

            serializedObject.ApplyModifiedProperties();

            if (GUILayout.Button("Change Navigation Tile"))
                IsTileSelected = false;

            EditorGUILayout.EndVertical();
        }

        #endregion
        #region SceneViewDrawers

        void DrawWaypoints()
        {
            var grid = Grid;
            var gridTransform = grid.transform;

            var navChunkData = NavChunk.Data;
            var chunkDataCount = navChunkData.Count;
            for (var i = 0; i < chunkDataCount; ++i)
            {
                var data = navChunkData[i];
                var tileData = data.TileData;
                if (tileData.IsWalkable)
                {
                    var worldPosition = grid.CellToWorld(data.Position) + tileData.Waypoint;
                    float size = HandleUtility.GetHandleSize(worldPosition) * 0.05f;

                    Handles.color = Color.blue;
                    Handles.DrawSolidDisc(worldPosition, -gridTransform.forward, size);
                }
            }
        }

        void SelectTileMode()
        {
            if (!IsEditMode || IsTileSelected)
                return;

            var grid = Grid;
            var positionOnGrid = CustomEditorUtility.GetCursorPositionOnGrid(grid);

            var cursorInBounds = grid.IsContains(positionOnGrid);
            var cursorColor = cursorInBounds ? Color.green : Color.red;

            CustomEditorUtility.DrawCursor(grid, positionOnGrid, cursorColor);

            var currentEvent = Event.current;
            switch (currentEvent.type)
            {
                case EventType.MouseDown:
                    {
                        if (currentEvent.button == 0 && cursorInBounds)
                        {
                            m_SelectedTilePosition = positionOnGrid;
                            IsTileSelected = true;
                        }

                        Event.current.Use();
                    }
                    break;

                case EventType.MouseMove:
                    SceneView.RepaintAll();
                    break;
            }
        }

        void DrawWaypointHandles()
        {
            if (!IsTileSelected)
                return;

            if (!m_IsWalkable.boolValue)
                return;

            serializedObject.Update();

            var grid = Grid;
            
            var cellPosition = grid.CellToWorld(m_SelectedTilePosition);
            var position = cellPosition + m_Waypoint.vector3Value;

            m_Waypoint.vector3Value =
                Handles.PositionHandle(cellPosition + m_Waypoint.vector3Value, Quaternion.identity) - cellPosition;
            
            float radius = HandleUtility.GetHandleSize(position) * 0.05f;
            Handles.color = Color.blue;
            Handles.DrawSolidDisc(position, -grid.transform.forward, radius);
            
            serializedObject.ApplyModifiedProperties();
        }

        #endregion
        #region Style

        class Style
        {
            public static readonly GUIStyle BOX_FRAME = new GUIStyle(GUI.skin.box);
            public static readonly GUIStyle BOLD_LABEL = EditorStyles.boldLabel;

            public static readonly GUIContent EDIT_MODE_BUTTON_LABEL = new GUIContent("Edit Navigation Data");
            public static readonly GUIContent EXIT_EDIT_MODE_BUTTON_LABEL = new GUIContent("Exit from 'Edit Mode'");

            public static readonly GUIContent SELECTED_NAV_TILE_DATA_LABEL = new GUIContent("Selected Nav Tile Data");
        }

        #endregion
    }
}