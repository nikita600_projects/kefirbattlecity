﻿using System.Collections.Generic;
using UnityEngine;

namespace nikita600Toolkit.Navigation
{
    /// <summary>
    /// Базовый класс, для компонентов, хранящих данные навигации.
    /// </summary>
    [DisallowMultipleComponent]
    public class NavigationChunk : MonoBehaviour
    {
        #region Fields
        
        [SerializeField]
        List<NavigationChunkData> m_Data = new List<NavigationChunkData>();  

        #endregion
        #region Methods

        public List<NavigationChunkData> Data
        {
            get { return m_Data; }
        } 

        #endregion
    }
}