﻿using UnityEngine;
using System;

namespace nikita600Toolkit.Navigation
{
    [Serializable]
    public struct NavigationChunkData
    {
        #region Fields

        [SerializeField]
        Vector2Int m_Position;

        [SerializeField]
        NavigationTile m_TileData;

        #endregion
        #region Properties

        public Vector2Int Position
        {
            get { return m_Position; }
        }

        public NavigationTile TileData
        {
            get { return m_TileData; }
        }

        #endregion
        #region Constructor

        public NavigationChunkData(Vector2Int _position, NavigationTile _tileData)
        {
            m_Position = _position;
            m_TileData = _tileData;
        }

        #endregion
    }
}