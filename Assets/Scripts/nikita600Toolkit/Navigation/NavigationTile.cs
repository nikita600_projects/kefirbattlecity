﻿using UnityEngine;
using System;

namespace nikita600Toolkit.Navigation
{
    [Serializable]
    public class NavigationTile
    {
        #region Fields
        
        [SerializeField]
        bool m_IsWalkable;

        [SerializeField]
        int m_Cost;

        [SerializeField]
        Vector3 m_Waypoint;

        #endregion
        #region Properties

        public bool IsWalkable
        {
            get { return m_IsWalkable; }
        }

        public int Cost
        {
            get { return m_Cost; }
        }
        
        public Vector3 Waypoint
        {
            get { return m_Waypoint; }
        }

        #endregion
    }
}