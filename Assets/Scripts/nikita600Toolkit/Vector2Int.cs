﻿using System;
using UnityEngine;

/// <summary>
/// Представление целочисленных 2D векторов и точек. 
/// </summary>
[Serializable]
public struct Vector2Int
{
    #region Fields

    /// <summary>
    /// x компонента вектора.
    /// </summary>
    public int x;

    /// <summary>
    /// y компонент вектора.
    /// </summary>
    public int y;

    #endregion
    #region Constructor

    public Vector2Int(int _x, int _y)
    {
        x = _x;
        y = _y;
    }

    #endregion
    #region Shortcuts

    public static Vector2Int left
    {
        get
        {
            return new Vector2Int(-1, 0);
        }
    }

    public static Vector2Int right
    {
        get
        {
            return new Vector2Int(1, 0);
        }
    }

    public static Vector2Int up
    {
        get
        {
            return new Vector2Int(0, 1);
        }
    }

    public static Vector2Int down
    {
        get
        {
            return new Vector2Int(0, -1);
        }
    }

    public static Vector2Int one
    {
        get
        {
            return new Vector2Int(1, 1);
        }
    }
    
    public static Vector2Int zero
    {
        get
        {
            return new Vector2Int(0, 0);
        }
    }

    #endregion
    #region ObjectOverrides

    public override bool Equals(object obj)
    {
        if ((obj is Vector2Int) == false)
            return false;

        Vector2Int vector2Int = (Vector2Int)obj;
        if (this.x.Equals(vector2Int.x))
            return this.y.Equals(vector2Int.y);

        return false;
    }

    public override int GetHashCode()
    {
        return x.GetHashCode() ^ y.GetHashCode() << 2;
    }

    public override string ToString()
    {
        return string.Format("({0}, {1})", x, y);
    }

    #endregion
    #region Operators

    public static Vector2Int operator +(Vector2Int _a, Vector2Int _b)
    {
        int x = _a.x + _b.x;
        int y = _a.y + _b.y;

        return new Vector2Int(x, y);
    }

    public static Vector3 operator +(Vector2Int _a, Vector3 _b)
    {
        return new Vector2(_a.x + _b.x, _a.y + _b.y);
    }

    public static Vector2Int operator -(Vector2Int _a, Vector2Int _b)
    {
        int x = _a.x - _b.x;
        int y = _a.y - _b.y;

        return new Vector2Int(x, y);
    }

    public static Vector3 operator -(Vector2Int _a, Vector3 _b)
    {
        float x = _a.x - _b.x;
        float y = _a.y - _b.y;

        return new Vector3(x, y);
    }

    public static Vector3 operator -(Vector3 _a, Vector2Int _b)
    {
        float x = _a.x - _b.x;
        float y = _a.y - _b.y;

        return new Vector3(x, y);
    }

    public static Vector2Int operator *(Vector2Int _vector, int _multiplier)
    {
        int x = _vector.x * _multiplier;
        int y = _vector.y * _multiplier;

        return new Vector2Int(x, y);
    }

    public static Vector2Int operator *(Vector2Int _a, Vector2Int _b)
    {
        int x = _a.x * _b.x;
        int y = _a.y * _b.y;

        return new Vector2Int(x, y);
    }

    public static Vector2 operator*(Vector2Int _a, Vector2 _b)
    {
        float x = _a.x * _b.x;
        float y = _a.y * _b.y;

        return new Vector2(x, y);
    }
    
    public static bool operator ==(Vector2Int _a, Vector2Int _b)
    {
        bool xEquals = (_a.x == _b.x);
        bool yEquals = (_a.y == _b.y);

        return xEquals && yEquals;
    }

    public static bool operator !=(Vector2Int _a, Vector2Int _b)
    {
        bool xNotEquals = (_a.x != _b.x);
        bool yNotEquals = (_a.y != _b.y);

        return xNotEquals || yNotEquals;
    }

    #endregion
}
