﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace nikita600Toolkit.GridComponents
{
    [DisallowMultipleComponent]
    public abstract class Grid : MonoBehaviour
    {
        #region Events

        public UnityEvent OnDirty;
        
        #endregion
        #region Fields

        [SerializeField]
        Vector2 m_CellSize = Vector2.one;

        [SerializeField]
        Vector2Int m_Size = Vector2Int.one;

        [SerializeField]
        protected Vector3 m_Pivot;

        [SerializeField]
        Color m_Color = Color.white;

        #endregion Fields
        #region Properties

        /// <summary>
        /// Цвет сетки.
        /// 
        /// Пока что используется только в редакторе.
        /// </summary>
        public Color Color
        {
            get { return m_Color; }
        }

        /// <summary>
        /// Размер клетки.
        /// </summary>
        public Vector2 CellSize
        {
            get { return m_CellSize; }
        }

        /// <summary>
        /// Размер сетки.
        /// </summary>
        public Vector2Int Size
        {
            get { return m_Size; }
        }
        
        /// <summary>
        /// Позиция точки опоры сетки.
        /// </summary>
        public Vector3 Pivot
        {
            get { return m_Pivot; }
        }

        #endregion Properties
        #region Methods
        #region MonoBehaviour

        void OnValidate()
        {
            var x = Mathf.Clamp(m_Size.x, 1, int.MaxValue);
            var y = Mathf.Clamp(m_Size.y, 1, int.MaxValue);
            m_Size = new Vector2Int(x, y);

            m_Pivot = GetPivot();
        }

        void Reset()
        {
            m_Pivot = GetPivot();

            m_CellSize = Vector2.one;
            m_Size = Vector2Int.one;
        }

        #endregion

        /// <summary>
        /// Содержит ли сетка позицию.
        /// </summary>
        /// <param name="_position"></param>
        /// <returns></returns>
        public bool IsContains(Vector2Int _position)
        {
            var gridSize = Size;
            var xContains = (_position.x >= 0) && (_position.x < gridSize.x);
            var yContains = (_position.y >= 0) && (_position.y < gridSize.y);

            return xContains && yContains;
        }

        /// <summary>
        /// Помещается ли объект внутри сетки.
        /// </summary>
        /// <param name="_position"></param>
        /// <param name="_size"></param>
        /// <returns></returns>
        public bool IsInBounds(Vector2Int _basePosition, Grid _gridObject)
        {
            if (!IsContains(_basePosition))
                return false;

            var maxPosition = _basePosition + _gridObject.Size - Vector2Int.one;
            if (!IsContains(maxPosition))
                return false;

            return true;
        }

        /// <summary>
        /// Конвертация сеточных координат в локальные.
        /// </summary>
        /// <param name="_cellPosition"></param>
        /// <returns></returns>
        public abstract Vector3 CellToLocal(Vector2Int _cellPosition);

        /// <summary>
        /// Конвертация локальных координат в сеточные.
        /// </summary>
        /// <param name="_localPosition"></param>
        /// <returns></returns>
        public abstract Vector2Int LocalToCell(Vector3 _localPosition);

        /// <summary>
        /// Конвертация сеточных координат в мировые.
        /// </summary>
        /// <param name="_cellPosition"></param>
        /// <returns></returns>
        public abstract Vector3 CellToWorld(Vector2Int _cellPosition);

        /// <summary>
        /// Конвертация мировых координат в сеточные.
        /// </summary>
        /// <param name="_worldPosition"></param>
        /// <returns></returns>
        public abstract Vector2Int WorldToCell(Vector3 _worldPosition);

        /// <summary>
        /// Возвращает опорную точку сетки.
        /// </summary>
        /// <returns></returns>
        protected abstract Vector3 GetPivot();

        protected void SetDirty()
        {
            OnDirty.Invoke();
        }

        #endregion
    }
}