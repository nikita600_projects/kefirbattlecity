﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace nikita600Toolkit.GridComponents
{
    /// <summary>
    /// Реализация слоя контейнера префабов типа Grid.
    /// </summary>
    [DisallowMultipleComponent]
    public class GridLayer : MonoBehaviour
    {
        #region Events

        [HideInInspector]
        public UnityEvent OnDirty;

        #endregion
        #region Fields

        [SerializeField]
        List<GridLayerData> m_ObjectsOnLayerData = new List<GridLayerData>();

        Grid m_Grid;
        
        #endregion
        #region Properties

        /// <summary>
        /// Сетка, которой принадлежит слой.
        /// </summary>
        public Grid Grid
        {
            get
            {
                if (m_Grid == null)
                    m_Grid = GetComponentInParent<Grid>();

                return m_Grid;
            }
        }

        #endregion
        #region Methods
        
        /// <summary>
        /// Пересекается ли объект с другими объектами на сетке.
        /// </summary>
        /// <param name="_basePosition"></param>
        /// <param name="_object"></param>
        /// <returns></returns>
        public bool IsCollided(Vector2Int _basePosition, Grid _object)
        {
            if (_object == null)
                return false;

            var layerData = new GridLayerData(_basePosition, _object);
            var maxPosition = layerData.MaxPosition;
            
            var objectsOnLayerData = m_ObjectsOnLayerData;
            var objectsCount = objectsOnLayerData.Count;
            for (var i = 0; i < objectsCount; ++i)
            {
                var currentMetadata = objectsOnLayerData[i];
                if (currentMetadata.GridObject == null)
                    continue;

                var currentMinPosition = currentMetadata.BasePosition;
                var currentMaxPosition = currentMetadata.MaxPosition;

                if (IsOverlap(_basePosition, maxPosition, currentMinPosition, currentMaxPosition))
                    return true;
            }

            return false;
        }

        /// <summary>
        /// Возвращает объект, занимающий позицию.
        /// </summary>
        /// <param name="_position"></param>
        /// <returns></returns>
        public Grid GetObject(Vector2Int _position, out int _indexInList)
        {
            _indexInList = default(int);

            if (!Grid.IsContains(_position))
                return default(Grid);

            var maxPosition = _position + Vector2Int.one;

            var objectsOnLayerData = m_ObjectsOnLayerData;
            var objectsCount = objectsOnLayerData.Count;
            for (var i = 0; i < objectsCount; ++i)
            {
                var currentMetadata = objectsOnLayerData[i];
                if (currentMetadata.GridObject == null)
                    continue;

                var currentMinPosition = currentMetadata.BasePosition;
                var currentMaxPosition = currentMetadata.MaxPosition;

                if (IsOverlap(_position, maxPosition, currentMinPosition, currentMaxPosition))
                {
                    _indexInList = i;
                    return currentMetadata.GridObject;
                }
            }

            return default(Grid);
        }

        /// <summary>
        /// Установка объекта на сетку.
        /// </summary>
        /// <param name="_basePosition">Базовая позиция.</param>
        /// <param name="_object">Объект, установленный на сетку.</param>
        /// <returns></returns>
        public bool SetObject(Vector2Int _basePosition, Grid _object)
        {
            var grid = Grid;
            if (!grid.IsContains(_basePosition) || !grid.IsInBounds(_basePosition, _object) || IsCollided(_basePosition, _object))
                return false;
            
            var objectTransform = _object.transform;
            objectTransform.SetParent(transform);

            SetObjectLocalPosition(_basePosition, _object);

            var layerData = new GridLayerData(_basePosition, _object);
            m_ObjectsOnLayerData.Add(layerData);

            SetDirty();
            return true;
        }

        /// <summary>
        /// Удалить объект из списка объектов и иерархии.
        /// </summary>
        /// <param name="_position"></param>
        /// <returns></returns>
        public bool RemoveObject(Vector2Int _position, out Grid _gridObject)
        {
            var objectIndex = default(int);
            var gridObject = GetObject(_position, out objectIndex);
            _gridObject = gridObject;

            if (gridObject == null)
                return false;

            m_ObjectsOnLayerData.RemoveAt(objectIndex);

            SetDirty();
            return true;
        }

        /// <summary>
        /// Обновление положений объектов на сетке.
        /// </summary>
        public void UpdateObjectsPositions()
        {
            var layerObjects = m_ObjectsOnLayerData;
            var layerObjectsCount = layerObjects.Count;
            for (var i = 0; i < layerObjectsCount; ++i)
            {
                var layerData = layerObjects[i];
                SetObjectLocalPosition(layerData.BasePosition, layerData.GridObject);
            }

            SetDirty();
        }
        
        /// <summary>
        /// Пересекаются ли границы.
        /// </summary>
        /// <param name="_firstMin"></param>
        /// <param name="_firstMax"></param>
        /// <param name="_secondMin"></param>
        /// <param name="_secondMax"></param>
        /// <returns></returns>
        bool IsOverlap(Vector2Int _firstMin, Vector2Int _firstMax, Vector2Int _secondMin, Vector2Int _secondMax)
        {
            var flag1 = _firstMin.x < _secondMax.x;
            var flag2 = _firstMax.x > _secondMin.x;
            var flag3 = _firstMin.y < _secondMax.y;
            var flag4 = _firstMax.y > _secondMin.y;

            return flag1 && flag2 && flag3 && flag4;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="_basePosition"></param>
        /// <param name="_gridObject"></param>
        void SetObjectLocalPosition(Vector2Int _basePosition, Grid _gridObject)
        {
            var position = Grid.CellToLocal(_basePosition) + _gridObject.Pivot;
            var objectTransform = _gridObject.transform;
            objectTransform.localPosition = position;
        }

        void SetDirty()
        {
            OnDirty.Invoke();
        }
        
        #endregion
    }
}