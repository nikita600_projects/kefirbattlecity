﻿using UnityEngine;

namespace nikita600Toolkit.GridComponents
{
    /// <summary>
    /// Реализация прямоугольной сетки.
    /// </summary>
    public sealed class RectangleGrid : Grid
    {
        #region Methods

        /// <summary>
        /// 
        /// </summary>
        /// <param name="_cellPosition"></param>
        /// <returns></returns>
        public override Vector3 CellToLocal(Vector2Int _cellPosition)
        {
            var pivot = Pivot;
            var worldCell = _cellPosition * CellSize;
            
            var x = worldCell.x - pivot.x;
            var y = worldCell.y - pivot.y;

            return new Vector3(x, y);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="_cellPosition"></param>
        /// <returns></returns>
        public override Vector3 CellToWorld(Vector2Int _cellPosition)
        {
            var localPosition = CellToLocal(_cellPosition);
            return transform.TransformPoint(localPosition);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="_localPosition"></param>
        /// <returns></returns>
        public override Vector2Int LocalToCell(Vector3 _localPosition)
        {
            _localPosition += Pivot;

            var cellSize = CellSize;
            var position = new Vector3(_localPosition.x / cellSize.x, _localPosition.y / cellSize.y);
            
            var x = Mathf.FloorToInt(position.x);
            var y = Mathf.FloorToInt(position.y);
            
            return new Vector2Int(x, y);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="_worldPosition"></param>
        /// <returns></returns>
        public override Vector2Int WorldToCell(Vector3 _worldPosition)
        {
            var local = transform.InverseTransformPoint(_worldPosition);
            return LocalToCell(local);
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        protected override Vector3 GetPivot()
        {
            var maxPoint = Size * CellSize;
            var bounds = new Vector3(maxPoint.x, maxPoint.y);
            var pivot = bounds * 0.5f;

            return pivot;
        }

        #endregion
    }
}