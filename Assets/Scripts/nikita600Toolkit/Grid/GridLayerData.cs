﻿using System;
using UnityEngine;

namespace nikita600Toolkit.GridComponents
{
    [Serializable]
    public struct GridLayerData
    {
        #region Fields

        [SerializeField]
        Vector2Int m_BasePosition;

        [SerializeField]
        Grid m_GridObject;

        #endregion
        #region Properties

        public Grid GridObject
        {
            get { return m_GridObject; }
        }

        public Vector2Int BasePosition
        {
            get { return m_BasePosition; }
        }

        public Vector2Int MaxPosition
        {
            get
            {
                if (m_GridObject != null)
                    return m_BasePosition + m_GridObject.Size;

                return m_BasePosition;
            }
        }

        #endregion
        #region Constructor

        public GridLayerData(Vector2Int _basePosition, Grid _object)
        {
            m_BasePosition = _basePosition;
            m_GridObject = _object;
        }

        #endregion
    }
}