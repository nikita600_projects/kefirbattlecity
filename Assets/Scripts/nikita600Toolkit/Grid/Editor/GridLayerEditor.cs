﻿using nikita600Toolkit.EditorTools;
using UnityEngine;
using UnityEditor;
using UnityEditor.SceneManagement;

namespace nikita600Toolkit.GridComponents
{
    [CustomEditor(typeof(GridLayer))]
    public class GridLayerEditor : Editor
    {
        #region Fields

        GridLayer m_GridLayer;

        Mesh m_GridMesh;
        Material m_GridMaterial;

        PrefabPalette m_GridPrefabPalette;
        Object m_SelectedObject;

        Grid m_DragObject;
        Mesh m_DragObjectMeshGreen;
        Mesh m_DrawObjectMeshRed;

        Tool m_LastTool;
        ViewTool m_LastViewTool;

        #endregion
        #region Properties

        Grid Grid
        {
            get
            {
                return Layer.Grid;
            }
        }

        GridLayer Layer
        {
            get
            {
                if (m_GridLayer == null)
                    m_GridLayer = target as GridLayer;

                return m_GridLayer;
            }
        }

        Mesh GridMesh
        {
            get
            {
                if (m_GridMesh == null)
                {
                    var grid = Grid;
                    m_GridMesh = CustomEditorUtility.CreateGridMesh(grid, grid.Color);
                }

                return m_GridMesh;
            }
        }

        Material GridMaterial
        {
            get
            {
                if (m_GridMaterial == null)
                {
                    m_GridMaterial = CustomEditorUtility.WireMaterial;
                }

                return m_GridMaterial;
            }
        }

        int PassiveControlId
        {
            get { return GUIUtility.GetControlID(FocusType.Passive); }
        }

        #endregion
        #region Methods
        #region Editor

        void OnEnable()
        {
            Validate();

            m_GridPrefabPalette = PrefabPalette.Create<Grid>();
            m_GridPrefabPalette.OnSelectedAssetChanged += OnSelectedAssetChanged_Handler;

            m_LastTool = Tools.current;
            m_LastViewTool = Tools.viewTool;

            Tools.current = Tool.View;
            Tools.viewTool = ViewTool.FPS;
        }
        
        void OnDisable()
        {
            m_GridPrefabPalette.OnSelectedAssetChanged -= OnSelectedAssetChanged_Handler;

            if (m_DragObject != null)
                DestroyImmediate(m_DragObject.gameObject);

            Tools.current = m_LastTool;
            Tools.viewTool = m_LastViewTool;
        }

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            m_GridPrefabPalette.OnGUI();
        }

        /// <summary>
        /// Отрисовк UI на сцене.
        /// </summary>
        void OnSceneGUI()
        {
            var grid = Grid;
            var layerTransform = Layer.transform;
            var gridMaterial = GridMaterial;
            CustomEditorUtility.DrawMesh(layerTransform, GridMesh, gridMaterial);

            var cursorPositionOnGrid = CustomEditorUtility.GetCursorPositionOnGrid(grid);
            var gridContainsPosition = grid.IsContains(cursorPositionOnGrid);
            
            if (m_DragObject != null)
            {
                var inBounds = grid.IsInBounds(cursorPositionOnGrid, m_DragObject);
                var collided = Layer.IsCollided(cursorPositionOnGrid, m_DragObject);
                var mesh = gridContainsPosition && !collided && inBounds ? m_DragObjectMeshGreen : m_DrawObjectMeshRed;
                var dragObjectTransform = m_DragObject.transform;
                CustomEditorUtility.DrawMesh(dragObjectTransform.position, dragObjectTransform.rotation, mesh, gridMaterial);
            }
            else
            {
                var cursorColor = gridContainsPosition ? Color.green : Color.red;
                CustomEditorUtility.DrawCursor(grid, layerTransform, gridMaterial, cursorPositionOnGrid, cursorColor);
            }

            var currentEvent = Event.current;

            MouseMoveHandler(currentEvent, cursorPositionOnGrid);
            MouseDragHandler(currentEvent, cursorPositionOnGrid);

            LeftMouseButtonHandler(currentEvent, cursorPositionOnGrid);
            RightMouseButtonHandler(currentEvent, cursorPositionOnGrid);

            if (currentEvent.type == EventType.MouseMove)
                SceneView.RepaintAll();
        }

        #endregion Editor
        
        /// <summary>
        /// Обработчик "перетягивания" мыши на сетке.
        /// </summary>
        /// <param name="_event"></param>
        /// <param name="_cursorPositionOnGrid"></param>
        void MouseDragHandler(Event _event, Vector2Int _cursorPositionOnGrid)
        {
            if (_event.type == EventType.MouseDrag && _event.isMouse)
            {
                if (_event.button == 0)
                {
                    SetObjectOnLayer(_cursorPositionOnGrid);
                    
                    GUIUtility.hotControl = PassiveControlId;
                    Event.current.Use();
                }
                else if (_event.button == 1)
                {
                    RemoveObjectFromLayer(_cursorPositionOnGrid);
                    
                    GUIUtility.hotControl = PassiveControlId;
                    Event.current.Use();
                }
            }
        }

        /// <summary>
        /// Перемещение мыши на сетке.
        /// </summary>
        /// <param name="_event"></param>
        /// <param name="_cursorPositionOnGrid"></param>
        void MouseMoveHandler(Event _event, Vector2Int _cursorPositionOnGrid)
        {
            if (_event.type != EventType.MouseMove || m_DragObject == null)
                return;

            var grid = Grid;
            var worldPosition = grid.CellToWorld(_cursorPositionOnGrid);
            worldPosition += grid.transform.TransformPoint(m_DragObject.Pivot);
            
            m_DragObject.transform.position = worldPosition;
        }

        /// <summary>
        /// Обработчик нажатия левой кнопки мыши.
        /// </summary>
        /// <param name="_event"></param>
        /// <param name="_cursorPositionOnGrid"></param>
        void LeftMouseButtonHandler(Event _event, Vector2Int _cursorPositionOnGrid)
        {
            if (_event.button != 0 || _event.type != EventType.MouseDown)
                return;

            SetObjectOnLayer(_cursorPositionOnGrid);

            GUIUtility.hotControl = PassiveControlId;
            Event.current.Use();
        }

        /// <summary>
        /// Обработчик нажатия правой кнопки мыши.
        /// </summary>
        /// <param name="_event"></param>
        /// <param name="_cursorPositionOnGrid"></param>
        void RightMouseButtonHandler(Event _event, Vector2Int _cursorPositionOnGrid)
        {
            if (_event.button != 1 || _event.type != EventType.MouseDown)
                return;

            RemoveObjectFromLayer(_cursorPositionOnGrid);

            GUIUtility.hotControl = PassiveControlId;
            Event.current.Use();
        }
        
        /// <summary>
        /// 
        /// </summary>
        void Validate()
        {
            if (Grid != null)
                return;

            if (EditorUtility.DisplayDialog("Error!", "'Grid Layer' must be a child of 'Grid' object.\n" +
                                                      "Press OK, to delete GridLayer component.", "OK", "Cancel"))
                DestroyImmediate(Layer);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="_selectedObject"></param>
        void OnSelectedAssetChanged_Handler(Object _selectedObject)
        {
            m_SelectedObject = _selectedObject;

            if (m_DragObject != null)
            {
                DestroyImmediate(m_DragObject.gameObject);
            }

            if (m_SelectedObject == null)
                return;

            m_DragObject = InstantiatePrefab<Grid>(m_SelectedObject, HideFlags.HideInHierarchy);
            m_DragObjectMeshGreen = CustomEditorUtility.CreateGridMesh(m_DragObject, Color.green);
            m_DrawObjectMeshRed = CustomEditorUtility.CreateGridMesh(m_DragObject, Color.red);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="_object"></param>
        /// <param name="_flags"></param>
        /// <returns></returns>
        T InstantiatePrefab<T>(Object _object, HideFlags _flags) where T : MonoBehaviour
        {
            var prefab = PrefabUtility.InstantiatePrefab(_object) as GameObject;
            if (prefab != null)
            {
                prefab.hideFlags = _flags;

                var typeObject = prefab.GetComponent<T>();
                if (typeObject == null)
                    DestroyImmediate(prefab);
                else
                    return typeObject;
            }

            return default(T);
        }

        /// <summary>
        /// Установка объекта на сетке.
        /// </summary>
        /// <param name="_cursorPositionOnGrid"></param>
        void SetObjectOnLayer(Vector2Int _cursorPositionOnGrid)
        {
            if (!Grid.IsContains(_cursorPositionOnGrid) || Layer.IsCollided(_cursorPositionOnGrid, m_DragObject))
                return;

            var gridPrefab = InstantiatePrefab<Grid>(m_SelectedObject, HideFlags.None);
            if (gridPrefab == null)
                return;

            if (Layer.SetObject(_cursorPositionOnGrid, gridPrefab))
            {
                SetSceneDirty();
                //var undoDescription = string.Format("Set Instantiate on Grid: {0}", target.name);
                //Undo.RegisterCreatedObjectUndo(gridPrefab, undoDescription);
            }
            else
            {
                DestroyImmediate(gridPrefab.gameObject);
            }
        }

        /// <summary>
        /// Удаление объекта из слоя.
        /// </summary>
        /// <param name="_cursorPositionOnGrid"></param>
        void RemoveObjectFromLayer(Vector2Int _cursorPositionOnGrid)
        {
            //var undoDescription = string.Format("Remove Instantiate on Layer: {0}", target.name);
            //Undo.RecordObject(target, undoDescription);

            Grid removedObject;
            if (Layer.RemoveObject(_cursorPositionOnGrid, out removedObject))
            {
                //Undo.DestroyObjectImmediate(removedObject);
                DestroyImmediate(removedObject.gameObject);

                SetSceneDirty();
            }
        }

        void SetSceneDirty()
        {
            var currentScene = EditorSceneManager.GetActiveScene();
            EditorSceneManager.MarkSceneDirty(currentScene);
        }

        #endregion Methods
    }
}