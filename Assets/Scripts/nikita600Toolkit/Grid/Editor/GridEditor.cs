﻿using nikita600Toolkit.EditorTools;
using UnityEngine;
using UnityEditor;

namespace nikita600Toolkit.GridComponents
{
    [CustomEditor(typeof(Grid), true)]
    public class GridEditor : Editor
    {
        #region Fields

        Grid m_Grid;
        Mesh m_GridMesh;
        Material m_GridMaterial;

        SerializedProperty m_Script;
        SerializedProperty m_Color;
        SerializedProperty m_Pivot;
        SerializedProperty m_Size;
        SerializedProperty m_CellSize;
        SerializedProperty m_CellPivot;

        #endregion
        #region Properties

        Grid Grid
        {
            get
            {
                if (m_Grid == null)
                    m_Grid = target as Grid;

                return m_Grid;
            }
        }

        Mesh GridMesh
        {
            get
            {
                if (m_GridMesh == null)
                {
                    var grid = Grid;
                    m_GridMesh = CustomEditorUtility.CreateGridMesh(grid, grid.Color);
                }
                
                return m_GridMesh;
            }
        }

        Material GridMaterial
        {
            get
            {
                if (m_GridMaterial == null)
                    m_GridMaterial = CustomEditorUtility.WireMaterial;

                return m_GridMaterial;
            }
        }

        #endregion
        #region Methods
        #region Editor

        void OnEnable()
        {
            m_Script = CustomEditorUtility.GetScriptProperty(serializedObject);

            m_CellSize = serializedObject.FindProperty("m_CellSize");

            m_Size = serializedObject.FindProperty("m_Size");
            m_Pivot = serializedObject.FindProperty("m_Pivot");

            m_Color = serializedObject.FindProperty("m_Color");
        }

        public override void OnInspectorGUI()
        {
            CustomEditorUtility.DrawDisabledField(m_Script);

            EditorGUILayout.LabelField(Style.GRID_PROPERTIES_LABEL, Style.BOLD_LABEL);

            serializedObject.Update();

            EditorGUI.BeginChangeCheck();

            EditorGUILayout.PropertyField(m_CellSize);
            EditorGUILayout.PropertyField(m_Size);

            serializedObject.ApplyModifiedProperties();

            if (EditorGUI.EndChangeCheck())
            {
                if (m_GridMesh != null)
                {
                    DestroyImmediate(m_GridMesh);
                    m_GridMesh = null;
                }

                UpdateLayers();
            }

            CustomEditorUtility.DrawDisabledField(m_Pivot);
            EditorGUILayout.PropertyField(m_Color);

            serializedObject.ApplyModifiedProperties();
        }

        void OnSceneGUI()
        {
            CustomEditorUtility.DrawMesh(Grid.transform, GridMesh, GridMaterial);
        }

        #endregion Editor

        void UpdateLayers()
        {
            var layers = Grid.GetComponentsInChildren<GridLayer>();
            var layersCount = layers.Length;

            for (var i = 0; i < layersCount; ++i)
            {
                var currentLayer = layers[i];
                currentLayer.UpdateObjectsPositions();
            }
        }

        #endregion Methods
        #region Style

        class Style
        {
            public static readonly GUIStyle BOLD_LABEL = new GUIStyle(EditorStyles.boldLabel);
            public static readonly GUIContent GRID_PROPERTIES_LABEL = new GUIContent("Grid Properties");
        }

        #endregion
    }
}