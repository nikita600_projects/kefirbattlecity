﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace nikita600Toolkit.SceneSystem
{
    public sealed class SceneLoader : MonoBehaviour
    {
        #region Fields

        [SerializeField]
        SceneEntry m_SceneEntry;

        #endregion
        #region Methods

        public void LoadScene()
        {
            var scenePath = m_SceneEntry.ScenePath;
            SceneManager.LoadScene(scenePath);
        }

        #endregion
    }
}