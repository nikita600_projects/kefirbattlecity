﻿using UnityEngine;

namespace nikita600Toolkit.SceneSystem
{
    [System.Serializable]
    struct SceneEntry
#if UNITY_EDITOR
            : ISerializationCallbackReceiver
#endif
    {
        #region Fields

        [SerializeField]
        string m_ScenePath;
        
        #endregion
        #region Properties

        public string ScenePath
        {
            get { return m_ScenePath; }
        }

        #endregion
        #region Methods

#if UNITY_EDITOR

        static readonly string ASSETS_FOLDER = "Assets/";
        static readonly string SCENE_EXTENSION = ".unity";

        [SerializeField]
        Object m_Scene;
        
        void ISerializationCallbackReceiver.OnBeforeSerialize()
        {
            if (m_Scene != null)
            {
                var assetPath = UnityEditor.AssetDatabase.GetAssetPath(m_Scene);
                var isSceneAsset = !string.IsNullOrEmpty(assetPath) && assetPath.Contains(SCENE_EXTENSION);
                if (isSceneAsset)
                {
                    var relativeScenePath = assetPath.Replace(ASSETS_FOLDER, string.Empty);
                    var scenePathWitoutExtension = relativeScenePath.Replace(SCENE_EXTENSION, string.Empty);

                    m_ScenePath = scenePathWitoutExtension;

                    return;
                }
            }

            m_Scene = null;
            m_ScenePath = string.Empty;
        }
        
        void ISerializationCallbackReceiver.OnAfterDeserialize() { }

#endif

        #endregion
    }
}