﻿#region EditorOnlyCode
#if UNITY_EDITOR
using UnityEditor;
#endif
#endregion EditorOnlyCode

using UnityEngine;

namespace nikita600Toolkit.AssetSystem
{
    [System.Serializable]
    public sealed class AssetMetadata
    #region EditorOnlyCode
#if UNITY_EDITOR
        : ISerializationCallbackReceiver
#endif
    #endregion EditorOnlyCode
    {
        #region EditorOnlyCode
#if UNITY_EDITOR

        [SerializeField]
        Object m_Object;

        void ResetData()
        {
            m_Object = null;
            m_AssetPath = string.Empty;
            m_AssetBundleName = string.Empty;
        }

        void ISerializationCallbackReceiver.OnAfterDeserialize() { }

        void ISerializationCallbackReceiver.OnBeforeSerialize()
        {
            if (m_Object != null)
            {
                var assetPath = AssetDatabase.GetAssetPath(m_Object);
                var isMainAsset = AssetDatabase.IsMainAsset(m_Object);
                if (!string.IsNullOrEmpty(assetPath) && isMainAsset)
                {
                    var assetImporter = AssetImporter.GetAtPath(assetPath);
                    m_AssetPath = assetImporter.assetPath;
                    m_AssetBundleName = assetImporter.assetBundleName;

                    if (!string.IsNullOrEmpty(m_AssetBundleName))
                        return;
                }
            }

            ResetData();
        }

#endif
        #endregion EditorOnlyCode
        #region Fields

        [SerializeField]
        string m_AssetPath;

        [SerializeField]
        string m_AssetBundleName;

        AssetBundleRequest m_AssetRequest;

        #endregion Fields
        #region Properties

        public string AssetPath
        {
            get { return m_AssetPath; }
        }

        public string AssetBundleName
        {
            get { return m_AssetBundleName; }
        }

        public AssetBundleRequest AssetRequest
        {
            get { return m_AssetRequest; }
            set { m_AssetRequest = value; }
        }

        #endregion Properties
    }
}