﻿using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEditor;

namespace nikita600Toolkit.AssetSystem
{
    /// <summary>
    /// Окно редактора, отвечающее за сборку ресурсов в бандлы. 
    /// </summary>
    public sealed class AssetBundleBuilder : EditorWindow
    {
        #region Fields

        bool m_BuildAllAssetBundlesFlag;

        string m_OutputPath = "Assets/StreamingAssets";

        BuildAssetBundleOptions m_BuildAssetBundleOptions = BuildAssetBundleOptions.UncompressedAssetBundle;

        BuildTarget m_BuildTarget = BuildTarget.StandaloneWindows;

        readonly List<AssetBundleMapEntry> m_AssetBundleMap = new List<AssetBundleMapEntry>();

        #endregion
        #region Methods

        [MenuItem("nikita600Toolkit/AssetBundle Builder")]
        static void ShowWindow()
        {
            var editorWindow = GetWindow<AssetBundleBuilder>(false, "AssetBundle Builder", true);
            editorWindow.Show();
        }

        void OnEnable()
        {
            UpdateAssetBundleMap();

            AssetBundlesMetadataHook.OnAssetBundleNameListChanged += UpdateAssetBundleMap;

        }

        void OnDisable()
        {
            AssetBundlesMetadataHook.OnAssetBundleNameListChanged -= UpdateAssetBundleMap;
        }

        void OnGUI()
        {
            DrawSelectOutputPath();

            DrawAssetBundlesMap();

            DrawBuildAssetBundlesButton();
        }

        void UpdateAssetBundleMap()
        {
            var allAssetBundleNames = AssetDatabase.GetAllAssetBundleNames();

            var unusedAssetBundleNames = AssetDatabase.GetUnusedAssetBundleNames();
            var unusedAssetBundleNamesList = new List<string>(unusedAssetBundleNames);

            m_AssetBundleMap.Clear();

            foreach (var assetBundleName in allAssetBundleNames)
            {
                if (unusedAssetBundleNamesList.Contains(assetBundleName))
                    continue;

                var assetBundleMapEntry = new AssetBundleMapEntry(assetBundleName);
                m_AssetBundleMap.Add(assetBundleMapEntry);
            }

            Repaint();
        }

        #endregion
        #region UI

        void DrawSelectOutputPath()
        {
            EditorGUILayout.BeginVertical(Style.BOX);

            GUILayout.Label(Style.OUTPUT_PATH_LABEL, Style.BOLD_LABEL);

            EditorGUILayout.BeginHorizontal();
            m_OutputPath = EditorGUILayout.TextArea(m_OutputPath);
            if (GUILayout.Button(Style.CHANGE_OUTPUT_PATH_BUTTON_LABEL))
            {
                m_OutputPath = EditorUtility.OpenFolderPanel("Select output path to asset bundles...", m_OutputPath, string.Empty);
                m_OutputPath = FileUtil.GetProjectRelativePath(m_OutputPath);
            }
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.EndVertical();
        }

        void DrawAssetBundlesMap()
        {
            EditorGUILayout.BeginVertical(Style.BOX);
            GUILayout.Label(Style.ASSET_BUNDLE_MAP_LABEL, Style.BOLD_LABEL);

            // TEMP
            m_BuildTarget = (BuildTarget)EditorGUILayout.EnumPopup("Platform", m_BuildTarget);
            m_BuildAssetBundleOptions = (BuildAssetBundleOptions) EditorGUILayout.EnumPopup("Build Options", m_BuildAssetBundleOptions);
            
            var assetBundleMapEntryCount = m_AssetBundleMap.Count;
            if (assetBundleMapEntryCount > 0)
            {
                EditorGUI.BeginChangeCheck();
                m_BuildAllAssetBundlesFlag = EditorGUILayout.ToggleLeft(Style.BUILD_ALL_ASSET_BUNDLES_LABEL, m_BuildAllAssetBundlesFlag, Style.BOLD_LABEL);
                if (EditorGUI.EndChangeCheck())
                {
                    m_AssetBundleMap.ForEach(assetBundleEntry =>
                    {
                        assetBundleEntry.Selected = m_BuildAllAssetBundlesFlag;
                    });
                }

                EditorGUILayout.BeginVertical(Style.BOX);

                for (var i = 0; i < assetBundleMapEntryCount; ++i)
                {
                    var abEntry = m_AssetBundleMap[i];
                    abEntry.Selected = EditorGUILayout.Toggle(abEntry.BundleName, abEntry.Selected);

                    if (m_BuildAllAssetBundlesFlag && !abEntry.Selected)
                        m_BuildAllAssetBundlesFlag = false;
                }

                EditorGUILayout.EndVertical();
            }
            else
            {
                EditorGUILayout.HelpBox("Project does not contain any 'Assets' assigned to 'AssetBundle'.", MessageType.Info);
            }
            
            EditorGUILayout.EndVertical();
        }

        void DrawBuildAssetBundlesButton()
        {
            if (string.IsNullOrEmpty(m_OutputPath))
                return;

            if (!Directory.Exists(m_OutputPath))
                Directory.CreateDirectory(m_OutputPath);

            if (GUILayout.Button("Build AssetBundles"))
            {
                var buildMap = new List<AssetBundleBuild>();

                foreach (var assetBundleMapEntry in m_AssetBundleMap)
                {
                    if (assetBundleMapEntry.Selected)
                        buildMap.Add(new AssetBundleBuild()
                        {
                            assetBundleName = assetBundleMapEntry.BundleName
                        });
                }

                if (buildMap.Count > 0)
                    BuildPipeline.BuildAssetBundles(m_OutputPath, m_BuildAssetBundleOptions, m_BuildTarget);

                AssetDatabase.Refresh();
            }
        }

        #endregion
        #region AssetBundleMapEntry

        class AssetBundleMapEntry
        {
            public readonly string BundleName;
            public bool Selected;

            public AssetBundleMapEntry(string _bundleName)
            {
                BundleName = _bundleName;
            }
        }

        #endregion
        #region AssetBundlesMetadataHook

        class AssetBundlesMetadataHook : AssetPostprocessor
        {
            #region Events

            public static event System.Action OnAssetBundleNameListChanged;

            #endregion
            #region Methods

            void OnPostprocessAssetbundleNameChanged(string _assetPath, string _previousAssetBundleName, string _newAssetBundleName)
            {
                if (OnAssetBundleNameListChanged != null)
                    OnAssetBundleNameListChanged.Invoke();
            }

            #endregion
        }

        #endregion
        #region Style

        class Style
        {
            public static readonly GUIStyle BOX = new GUIStyle(GUI.skin.box);
            public static readonly GUIStyle BOLD_LABEL = new GUIStyle(EditorStyles.boldLabel);

            public static readonly GUIContent OUTPUT_PATH_LABEL = new GUIContent("Output path:");
            public static readonly GUIContent CHANGE_OUTPUT_PATH_BUTTON_LABEL = new GUIContent("Change Output Path");
            public static readonly string CHANGE_OUTPUT_PATH_WINDOW_LABEL = "Select Output Path...";

            public static readonly GUIContent ASSET_BUNDLE_MAP_LABEL = new GUIContent("Asset Bundle Map");
            public static readonly GUIContent BUILD_ALL_ASSET_BUNDLES_LABEL = new GUIContent("Build All Asset Bundles");
        }

        #endregion
    }
}