﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

namespace nikita600Toolkit.AssetSystem
{
    public sealed class AssetLoader
    {
        static readonly Dictionary<string, List<AssetMetadata>> s_SortedAssets = new Dictionary<string, List<AssetMetadata>>(); 

        public static void Load(string _path, List<AssetMetadata> _assets, Action<float> _onProgressChanged, Action<bool> _onLoadingFinished)
        {
            ClearSortedAssets();
            SortAssetMetadata(_assets);

            
        }

        static void ClearSortedAssets()
        {
            foreach (var assetList in s_SortedAssets)
                assetList.Value.Clear();
        }

        static void SortAssetMetadata(List<AssetMetadata> _asset)
        {
            var assetCount = _asset.Count;
            for (var i = 0; i < assetCount; ++i)
            {
                var assetMetadata = _asset[i];
                var assetBundleName = assetMetadata.AssetBundleName;

                if (!s_SortedAssets.ContainsKey(assetBundleName))
                    s_SortedAssets.Add(assetBundleName, new List<AssetMetadata>());

                var targetAssetsList = s_SortedAssets[assetBundleName];
                targetAssetsList.Add(assetMetadata);
            }
        }

        #region UnityHook
        
        class AssetLoaderUnityHook : MonoBehaviour
        {
            #region Fields

            static AssetLoaderUnityHook m_Instance;

            #endregion
            #region Methods

            [RuntimeInitializeOnLoadMethod]
            static void OnUnityStart()
            {
                var unityHookGameObject = new GameObject("AssetLoaderUnityHook");
                //unityHookGameObject.hideFlags = HideFlags.HideAndDontSave;

                DontDestroyOnLoad(unityHookGameObject);

                m_Instance = unityHookGameObject.AddComponent<AssetLoaderUnityHook>();
            }

            public static void StartAsyncLoad()
            {
                m_Instance.StartCoroutine(m_Instance.LoadAsset(null, null));
            }

            #endregion
            #region Coroutines
            
            IEnumerator LoadAsset(AssetBundle _assetBundle, string _assetPath)
            {


                yield return null;
            }

            #endregion
        }

        #endregion
    }
}