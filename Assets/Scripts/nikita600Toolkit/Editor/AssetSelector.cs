﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace nikita600Toolkit.EditorTools
{
    public abstract class PrefabPalette
    {
        #region Events

        event System.Action<Object> m_OnSelectedAssetChanged;

        #endregion
        #region Properties
        
        public virtual event System.Action<Object> OnSelectedAssetChanged
        {
            add { m_OnSelectedAssetChanged += value; }
            remove { m_OnSelectedAssetChanged -= value; }
        }

        #endregion
        #region Methods

        /// <summary>
        /// Создаёт новую палитру префабов для заданного типа скриптов.
        /// </summary>
        /// <typeparam name="T">Тип ассетов</typeparam>
        /// <returns></returns>
        public static PrefabPalette Create<T>() where T : MonoBehaviour
        {
            return new GenericPrefabPalette<T>();
        }

        public abstract void OnGUI();

        protected void SelectetAssetChanged(Object _object)
        {
            if (m_OnSelectedAssetChanged != null)
                m_OnSelectedAssetChanged(_object);
        }

        #endregion
        #region GenericAssetPalette

        sealed class GenericPrefabPalette<T> : PrefabPalette where T : MonoBehaviour
        {
            #region Constants

            static readonly string DEFAULT_ASSETS_PATH = "Assets";
            static readonly string FOLDERS_DEPTH_KEY = "FoldersDepth";
            static readonly string MAX_FOLDERS_COLUMNS_KEY = "MaxFoldersColumns";
            static readonly string MAX_PREFABS_COLUMNS_KEY = "MaxAssetsColumns";

            #endregion
            #region Fields

            bool m_IsViewOpened = true;

            int m_FoldersDepth = 3;

            int m_MaxFoldersColumns = 3;

            int m_MaxAssetsColumns = 3;

            string m_PathToAssets;

            Node m_RootNode;
            
            GUIContent m_TypeLabel = new GUIContent(string.Format("Type: {0}", typeof(T).ToString()));

            #endregion
            #region Properties

            public override event System.Action<Object> OnSelectedAssetChanged
            {
                add
                {
                    base.OnSelectedAssetChanged += value;

                    InvokeSelectedAssetChanged(m_RootNode);
                }
                remove { base.OnSelectedAssetChanged -= value; }
            }
            
            int FoldersDepth
            {
                get
                {
                    if (m_FoldersDepth != 0)
                        return m_FoldersDepth;

                    m_FoldersDepth = GetSavedInt(FOLDERS_DEPTH_KEY);

                    return m_FoldersDepth;
                }
                set
                {
                    m_FoldersDepth = value;
                    SetSavedInt(FOLDERS_DEPTH_KEY, m_FoldersDepth);
                }
            }

            int MaxFoldersColumns
            {
                get
                {
                    if (m_MaxFoldersColumns != 0)
                        return m_MaxFoldersColumns;

                    m_MaxFoldersColumns = GetSavedInt(MAX_FOLDERS_COLUMNS_KEY);

                    return m_MaxFoldersColumns;
                }
                set
                {
                    m_MaxFoldersColumns = value;
                    SetSavedInt(MAX_FOLDERS_COLUMNS_KEY, m_MaxFoldersColumns);
                }
            }

            int MaxAssetsColumns
            {
                get
                {
                    if (m_MaxAssetsColumns != 0)
                        return m_MaxAssetsColumns;

                    m_MaxAssetsColumns = GetSavedInt(MAX_PREFABS_COLUMNS_KEY);

                    return m_MaxAssetsColumns;
                }
                set
                {
                    m_MaxAssetsColumns = value;
                    SetSavedInt(MAX_PREFABS_COLUMNS_KEY, m_MaxAssetsColumns);
                }
            }

            string PathToAssets
            {
                get
                {
                    if (!string.IsNullOrEmpty(m_PathToAssets))
                        return m_PathToAssets;

                    var assetType = typeof(T).ToString();
                    m_PathToAssets = EditorPrefs.GetString(assetType);

                    if (string.IsNullOrEmpty(m_PathToAssets))
                        m_PathToAssets = DEFAULT_ASSETS_PATH;
                    
                    return m_PathToAssets;
                }
                set
                {
                    m_PathToAssets = value;

                    var assetType = typeof(T).ToString();
                    EditorPrefs.SetString(assetType, m_PathToAssets);
                    
                    UpdateNodeHierarchy();
                }
            }

            #endregion
            #region Constructor

            public GenericPrefabPalette()
            {
                m_PathToAssets = PathToAssets;
                UpdateNodeHierarchy();
            }

            #endregion
            #region Methods

            /// <summary>
            /// Отрисовка элементов UI.
            /// </summary>
            public override void OnGUI()
            {
                m_IsViewOpened = EditorGUILayout.Foldout(m_IsViewOpened, Style.PREFAB_PALETTE_LABEL);
                if (!m_IsViewOpened)
                    return;

                EditorGUILayout.BeginVertical(Style.BOX_FRAME);

                DrawMainHeader();
                DrawSettingsControls();

                EditorGUILayout.BeginVertical(Style.BOX_FRAME);
                // Отрисовка окна Folders
                DrawFoldersHeader();
                var node = DrawFoldersControls();
                // Отрисовка окна Assets
                DrawPrefabHeader();
                DrawPrefabControls(node);
                EditorGUILayout.EndVertical();

                EditorGUILayout.EndVertical();
            }

            /// <summary>
            /// 
            /// </summary>
            /// <param name="_name"></param>
            /// <returns></returns>
            int GetSavedInt(string _name)
            {
                var assetType = typeof(T).ToString();
                var prefKey = string.Format("{0}_{1}", assetType, _name);

                var intValue = EditorPrefs.GetInt(prefKey);

                intValue = Mathf.Clamp(intValue, 1, int.MaxValue);

                return intValue;
            }

            /// <summary>
            /// 
            /// </summary>
            /// <param name="_name"></param>
            /// <param name="_value"></param>
            void SetSavedInt(string _name, int _value)
            {
                _value = Mathf.Clamp(_value, 1, int.MaxValue);

                var assetType = typeof(T).ToString();
                var prefKey = string.Format("{0}_{1}", assetType, _name);

                EditorPrefs.SetInt(prefKey, _value);
            }

            /// <summary>
            /// 
            /// </summary>
            void UpdateNodeHierarchy()
            {
                var rootGuid = AssetDatabase.AssetPathToGUID(m_PathToAssets);
                m_RootNode = new Node(rootGuid, typeof(T));

                InvokeSelectedAssetChanged(m_RootNode);
            }

            #endregion
            #region UI_Drawers

            /// <summary>
            /// 
            /// </summary>
            void DrawMainHeader()
            {
                GUILayout.Label(Style.PREFAB_PALETTE_LABEL, Style.BOLD_LABEL);
                GUILayout.Label(m_TypeLabel, Style.BOLD_LABEL);
            }
            
            /// <summary>
            /// 
            /// </summary>
            void DrawFoldersHeader()
            {
                EditorGUILayout.BeginHorizontal();

                GUILayout.Label(Style.FOLDERS_LABEL, Style.BOLD_LABEL);

                EditorGUI.BeginChangeCheck();

                var maxFoldersColumns = MaxFoldersColumns;
                maxFoldersColumns = EditorGUILayout.IntField(Style.MAX_COLUMNS_PREFAB_LABEL, maxFoldersColumns);

                if (EditorGUI.EndChangeCheck())
                    MaxFoldersColumns = maxFoldersColumns;

                EditorGUILayout.EndHorizontal();
            }

            /// <summary>
            /// 
            /// </summary>
            void DrawSettingsControls()
            {
                EditorGUILayout.BeginHorizontal();

                // Folder Depth Field
                EditorGUI.BeginChangeCheck();
                var depth = FoldersDepth;
                depth = EditorGUILayout.IntField(Style.MAX_FOLDER_DEPTH_LABEL, depth);
                if (EditorGUI.EndChangeCheck())
                    FoldersDepth = depth;

                // Change Assets Folder Button
                string pathToAssets;
                if (ChangeAssetsFolderButton(out pathToAssets))
                    PathToAssets = pathToAssets;

                EditorGUILayout.EndHorizontal();
            }

            /// <summary>
            /// 
            /// </summary>
            /// <param name="_relativePath"></param>
            /// <returns></returns>
            bool ChangeAssetsFolderButton(out string _relativePath)
            {
                _relativePath = string.Empty;

                if (!GUILayout.Button(Style.CHANGE_PREFABS_ROOT_FOLDER_LABEL))
                    return false;

                var pathToAssets = PathToAssets;
                pathToAssets = EditorUtility.OpenFolderPanel(Style.SELECT_PREFABS_ROOT_FOLDER_LABEL, pathToAssets, pathToAssets);
                if (string.IsNullOrEmpty(pathToAssets))
                    return false;

                _relativePath = FileUtil.GetProjectRelativePath(pathToAssets);
                return true;
            }

            /// <summary>
            /// 
            /// </summary>
            /// <returns></returns>
            Node DrawFoldersControls()
            {
                var node = m_RootNode;
                if (node == null || !node.HasFolders)
                    return null;

                var foldersDepth = FoldersDepth;
                var maxFoldersColumns = MaxFoldersColumns;
                
                EditorGUI.BeginChangeCheck();

                for (var i = 0; i < foldersDepth; ++i)
                {
                    if (!node.HasFolders)
                        break;

                    EditorGUILayout.BeginVertical(Style.BOX_FRAME);

                    node.SelectedFolderIndex = GUILayout.SelectionGrid(node.SelectedFolderIndex, node.FoldersNames, maxFoldersColumns);
                    node = node.Folders[node.SelectedFolderIndex];

                    EditorGUILayout.EndVertical();
                }

                if (EditorGUI.EndChangeCheck())
                    InvokeSelectedAssetChanged(node);

                return node;
            }

            /// <summary>
            /// 
            /// </summary>
            void DrawPrefabHeader()
            {
                EditorGUILayout.BeginHorizontal();

                GUILayout.Label(Style.PREFABS_LABEL, Style.BOLD_LABEL);

                EditorGUI.BeginChangeCheck();

                var maxAssetsColumns = MaxAssetsColumns;
                maxAssetsColumns = EditorGUILayout.IntField(Style.MAX_COLUMNS_FOLDERS_LABEL, maxAssetsColumns);

                if (EditorGUI.EndChangeCheck())
                    MaxAssetsColumns = maxAssetsColumns;

                EditorGUILayout.EndHorizontal();
            }

            /// <summary>
            /// 
            /// </summary>
            /// <param name="_targetNode"></param>
            void DrawPrefabControls(Node _targetNode)
            {
                if (_targetNode == null || !_targetNode.HasAssets)
                    return;

                var maxAssetsColumns = MaxAssetsColumns;
                var assetPreviews = _targetNode.AssetPreviews;
                
                EditorGUILayout.BeginVertical(Style.BOX_FRAME);

                EditorGUI.BeginChangeCheck();

                _targetNode.SelectedAssetIndex = GUILayout.SelectionGrid(_targetNode.SelectedAssetIndex, assetPreviews, maxAssetsColumns, Style.PREVIEW_BUTTON_STYLE);

                if (EditorGUI.EndChangeCheck())
                    InvokeSelectedAssetChanged(_targetNode);

                EditorGUILayout.EndVertical();
            }

            void InvokeSelectedAssetChanged(Node _targetNode)
            {
                if (_targetNode == null || !_targetNode.HasAssets)
                    return;

                var assetIndex = _targetNode.SelectedAssetIndex;
                var selectedAsset = _targetNode.Assets[assetIndex].Asset;

                SelectetAssetChanged(selectedAsset);
            }

            #endregion
            #region Style

            /// <summary>
            /// Класс, хранящий визуальные стили редактора.
            /// </summary>
            class Style
            {
                public static readonly GUIStyle BOX_FRAME = new GUIStyle(GUI.skin.box);
                public static readonly GUIStyle BOLD_LABEL = EditorStyles.boldLabel;
                public static readonly GUIStyle PREVIEW_BUTTON_STYLE = new GUIStyle(GUI.skin.button)
                {
                    imagePosition = ImagePosition.ImageAbove
                };

                public static readonly GUIContent PREFAB_PALETTE_LABEL = new GUIContent("Prefab Palette");
                public static readonly GUIContent MAX_FOLDER_DEPTH_LABEL = new GUIContent("Max Folder Depth");
                public static readonly GUIContent FOLDERS_LABEL = new GUIContent("Folders");
                public static readonly GUIContent MAX_COLUMNS_FOLDERS_LABEL = new GUIContent("Max Columns in 'Folders'");
                public static readonly GUIContent PREFABS_LABEL = new GUIContent("Prefabs");
                public static readonly GUIContent MAX_COLUMNS_PREFAB_LABEL = new GUIContent("Max Columns in 'Prefabs'");

                public static readonly string CHANGE_PREFABS_ROOT_FOLDER_LABEL = "Change Prefabs Root Folder";
                public static readonly string SELECT_PREFABS_ROOT_FOLDER_LABEL = "Select Prefabs Root Folder...";
            }

            #endregion
            #region NodeModel

            /// <summary>
            /// Класс, описывающий структуру узла иерархии ассетов.
            /// </summary>
            sealed class Node
            {
                #region Constants

                static readonly string OBJECTS_SEARCH_PATTERN = "t:Prefab";

                #endregion
                #region Fields
                
                public readonly List<Node> Folders = new List<Node>();

                public readonly string[] FoldersNames;

                public readonly List<Node> Assets = new List<Node>();

                public readonly GUIContent[] AssetPreviews;

                readonly GUIContent m_Content;

                readonly string m_Guid;

                int m_SelectedFolderIndex;

                int m_SelectedAssetIndex;

                #endregion
                #region Properties

                public Object Asset
                {
                    get { return AssetDatabase.LoadMainAssetAtPath(Path); }
                }

                public bool HasFolders
                {
                    get { return Folders.Count > 0; }
                }

                public int SelectedFolderIndex
                {
                    get { return m_SelectedFolderIndex; }
                    set { m_SelectedFolderIndex = Mathf.Clamp(value, 0, int.MaxValue); }
                }

                public bool HasAssets
                {
                    get { return Assets.Count > 0; }
                }

                public int SelectedAssetIndex
                {
                    get { return m_SelectedAssetIndex; }
                    set { m_SelectedAssetIndex = Mathf.Clamp(value, 0, int.MaxValue); }
                }

                string Path
                {
                    get { return AssetDatabase.GUIDToAssetPath(m_Guid); }
                }
                
                #endregion
                #region Constructor

                public Node(string _guid, System.Type _assetType)
                {
                    m_Guid = _guid;
                    
                    var asset = Asset;
                    if (asset == null)
                        return;
                    
                    var preview = AssetPreview.GetAssetPreview(asset);
                    m_Content = new GUIContent(asset.name, preview);

                    var path = Path;
                    InitFoldersList(path, _assetType);

                    var foldersCount = Folders.Count;
                    if (foldersCount > 0)
                    {
                        FoldersNames = new string[foldersCount];

                        for (var i = 0; i < foldersCount; ++i)
                            FoldersNames[i] = Folders[i].m_Content.text;
                    }
                    
                    InitAssetsList(path, _assetType);

                    var assetCount = Assets.Count;
                    if (assetCount > 0)
                    {
                        AssetPreviews = new GUIContent[assetCount];

                        for (var i = 0; i < assetCount; ++i)
                            AssetPreviews[i] = Assets[i].m_Content;
                    }
                }

                #endregion
                #region Methods

                public override string ToString()
                {
                    return m_Content.text;
                }
                
                void InitFoldersList(string _path, System.Type _assetType)
                {
                    var subFoldersPaths = AssetDatabase.GetSubFolders(_path);

                    foreach (var subFolder in subFoldersPaths)
                    {
                        var guid = AssetDatabase.AssetPathToGUID(subFolder);
                        var node = new Node(guid, _assetType);

                        if (IsFolderContainsAssets(subFolder, _assetType))
                            Folders.Add(node);
                    }
                }

                void InitAssetsList(string _path, System.Type _assetType)
                {
                    var searchInFolders = new string[] { _path };
                    var assetGuids = AssetDatabase.FindAssets(OBJECTS_SEARCH_PATTERN, searchInFolders);

                    foreach (var assetGuid in assetGuids)
                    {
                        var assetPath = AssetDatabase.GUIDToAssetPath(assetGuid);
                        var prefab = AssetDatabase.LoadAssetAtPath<GameObject>(assetPath);
                        if (prefab == null)
                            continue;

                        var prefabType = prefab.GetComponent(_assetType);
                        if (prefabType == null)
                            continue;

                        var node = new Node(assetGuid, _assetType);
                        Assets.Add(node);
                    }
                }

                bool IsFolderContainsAssets(string _folder, System.Type _assetType)
                {
                    var searchInFolders = new string[] { _folder };
                    var assetGuids = AssetDatabase.FindAssets(OBJECTS_SEARCH_PATTERN, searchInFolders);

                    foreach (var assetGuid in assetGuids)
                    {
                        var assetPath = AssetDatabase.GUIDToAssetPath(assetGuid);
                        var prefab = AssetDatabase.LoadAssetAtPath<GameObject>(assetPath);
                        if (prefab == null)
                            continue;

                        var prefabType = prefab.GetComponent(_assetType);
                        if (prefabType == null)
                            continue;

                        return true;
                    }

                    return false;
                }

                #endregion
            }

            #endregion
        }

        #endregion
    }
}
