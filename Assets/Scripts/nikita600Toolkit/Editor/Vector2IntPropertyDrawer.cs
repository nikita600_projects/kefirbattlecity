﻿using UnityEngine;
using UnityEditor;

[CustomPropertyDrawer(typeof(Vector2Int))]
public class Vector2IntPropertyDrawer : PropertyDrawer
{
    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        return EditorGUIUtility.singleLineHeight;
    }

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        SerializedProperty xProp = property.FindPropertyRelative("x");
        SerializedProperty yProp = property.FindPropertyRelative("y");
        Vector2 value = new Vector2(xProp.intValue, yProp.intValue);

        value = EditorGUI.Vector2Field(position, label, value);

        int x = Mathf.FloorToInt(value.x);
        int y = Mathf.FloorToInt(value.y);
        xProp.intValue = x;
        yProp.intValue = y;
    }
}
