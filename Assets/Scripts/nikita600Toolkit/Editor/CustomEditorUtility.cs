﻿using nikita600Toolkit.GridComponents;
using UnityEngine;
using UnityEditor;

namespace nikita600Toolkit.EditorTools
{
    static class CustomEditorUtility
    {
        #region Constants

        static readonly float s_GridGizmoColorDistance = 500;
        
        static readonly Vector2 s_UV = new Vector2(s_GridGizmoColorDistance, 0);

        static readonly Vector3[] s_CursorLines = new Vector3[4];

        #endregion
        #region Fields

        static Material s_WireMaterial;

        #endregion
        #region Properties

        public static Material WireMaterial
        {
            get
            {
                if (s_WireMaterial == null)
                {
                    var wireShader = Shader.Find("Custom/Lines Colored Blended");
                    var wireMaterial = new Material(wireShader);

                    s_WireMaterial = wireMaterial;

                    //s_WireMaterial = EditorGUIUtility.LoadRequired("SceneView/Grid.mat") as Material;
                }

                return s_WireMaterial;
            }
        }

        #endregion
        #region Methods

        /// <summary>
        /// 
        /// </summary>
        /// <param name="_grid"></param>
        /// <returns></returns>
        public static Vector2Int GetCursorPositionOnGrid(Grid _grid)
        {
            var mousePosition = Event.current.mousePosition;
            var ray = HandleUtility.GUIPointToWorldRay(mousePosition);

            var gridTransform = _grid.transform;
            var plane = new Plane(gridTransform.forward * -1f, gridTransform.position);

            float enter;
            plane.Raycast(ray, out enter);

            var point = ray.GetPoint(enter);
            return _grid.WorldToCell(point);
        }

        /// <summary>
        /// Возвращает свойство, отвечающее за скрипт.
        /// </summary>
        /// <param name="_serializedObject"></param>
        /// <returns></returns>
        public static SerializedProperty GetScriptProperty(SerializedObject _serializedObject)
        {
            return _serializedObject.FindProperty("m_Script");
        }

        /// <summary>
        /// Отрисовка в инспекторе неактивное свойство.
        /// </summary>
        /// <param name="_serializedProperty"></param>
        public static void DrawDisabledField(SerializedProperty _serializedProperty)
        {
            GUI.enabled = false;
            EditorGUILayout.PropertyField(_serializedProperty, false);
            GUI.enabled = true;
        }

        /// <summary>
        /// Отрисовка меша.
        /// </summary>
        /// <param name="_position"></param>
        /// <param name="_rotation"></param>
        /// <param name="_mesh"></param>
        /// <param name="_material"></param>
        public static void DrawMesh(Vector3 _position, Quaternion _rotation, Mesh _mesh, Material _material)
        {
            if (Event.current.type != EventType.Repaint)
                return;

            if (_material == null || _mesh == null)
                return;

            _material.SetPass(0);

            GL.PushMatrix();
            GL.Begin(1);

            Graphics.DrawMeshNow(_mesh, _position, _rotation);

            GL.End();
            GL.PopMatrix();
        }

        /// <summary>
        /// Отрисовка меша.
        /// </summary>
        public static void DrawMesh(Transform _transform, Mesh _mesh, Material _material)
        {
            if (Event.current.type != EventType.Repaint)
                return;

            if (_transform == null || _material == null || _mesh == null)
                return;

            _material.SetPass(0);

            GL.PushMatrix();
            GL.Begin(1);
            
            Graphics.DrawMeshNow(_mesh, _transform.localToWorldMatrix);

            GL.End();
            GL.PopMatrix();
        }
        
        /// <summary>
        /// Отрисовка курсора.
        /// </summary>
        /// <param name="_position"></param>
        /// <param name="_grid"></param>
        /// <param name="_cursorMesh"></param>
        /// <param name="_gridMaterial"></param>
        public static void DrawCursor(Grid _grid, Transform _transform, Material _material, Vector2Int _position, Color _color)
        {
            if (Event.current.type != EventType.Repaint)
                return;

            if (_grid == null || _material == null || _transform == null)
                return;

            CalculateCursorLines(_grid, _position);

            _material.SetPass(0);

            GL.PushMatrix();
            GL.MultMatrix(_transform.localToWorldMatrix);
            GL.Begin(1);
            GL.Color(_color);

            int first = 0;
            int second = s_CursorLines.Length - 1;
            for (; first < s_CursorLines.Length; second = first++)
            {
                GL.Vertex(s_CursorLines[second]);
                GL.Vertex(s_CursorLines[first]);
            }

            GL.End();
            GL.PopMatrix();
        }

        public static void DrawCursor(Grid _grid, Vector2Int _position, Color _color)
        {
            DrawCursor(_grid, _grid.transform, WireMaterial, _position, _color);
        }

        /// <summary>
        /// Создание меша сетки.
        /// </summary>
        /// <param name="_grid"></param>
        /// <returns></returns>
        public static Mesh CreateGridMesh(Grid _grid, Color _color)
        {
            int index = 0;

            Vector2Int gridSize = _grid.Size;
            int vertexCount = ((gridSize.x + 1) + (gridSize.y + 1)) * 2;

            Mesh mesh = new Mesh();
            mesh.hideFlags = HideFlags.HideAndDontSave;

            int[] indices = new int[vertexCount];
            Vector3[] vertices = new Vector3[vertexCount];
            Vector2[] uvs = new Vector2[vertexCount];
            Color[] colors = new Color[vertexCount];

            for (int x = 0; x <= gridSize.x; ++x)
            {
                Vector2Int start = new Vector2Int(x, 0);
                AssignLineData(_grid, start, index, indices, vertices, uvs, colors, _color);
                ++index;

                Vector2Int end = new Vector2Int(x, gridSize.y);
                AssignLineData(_grid, end, index, indices, vertices, uvs, colors, _color);
                ++index;
            }

            for (int y = 0; y <= gridSize.y; ++y)
            {
                Vector2Int start = new Vector2Int(0, y);
                AssignLineData(_grid, start, index, indices, vertices, uvs, colors, _color);
                ++index;

                Vector2Int end = new Vector2Int(gridSize.x, y);
                AssignLineData(_grid, end, index, indices, vertices, uvs, colors, _color);
                ++index;
            }

            mesh.vertices = vertices;
            mesh.uv = uvs;
            mesh.colors = colors;
            mesh.SetIndices(indices, MeshTopology.Lines, 0);
            
            return mesh;
        }

        static void AssignLineData(Grid _grid, Vector2Int _cell, int _index, int[] indices, Vector3[] vertices, Vector2[] uvs, Color[] colors, Color _color)
        {
            vertices[_index] = _grid.CellToLocal(_cell);
            indices[_index] = _index;
            uvs[_index] = s_UV;
            colors[_index] = _color;
        }

        static void CalculateCursorLines(Grid _grid, Vector2Int _position)
        {
            if (_grid == null)
                return;

            s_CursorLines[0] = _grid.CellToLocal(_position);
            s_CursorLines[1] = _grid.CellToLocal(_position + Vector2Int.up);
            s_CursorLines[2] = _grid.CellToLocal(_position + (Vector2Int.right + Vector2Int.up));
            s_CursorLines[3] = _grid.CellToLocal(_position + Vector2Int.right);
        }

        #endregion
    }
}