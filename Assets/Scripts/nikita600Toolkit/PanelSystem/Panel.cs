﻿using UnityEngine;
using UnityEngine.Events;

namespace nikita600Toolkit.PanelSystem
{
    [RequireComponent(typeof(Animator))]
    public sealed class Panel : MonoBehaviour
    {
        #region Constants

        static readonly string IS_OPENED = "IsOpened";

        #endregion
        #region Fields

        [SerializeField, HideInInspector]
        Animator m_Animator;

        [SerializeField]
        bool m_OpenOnEnable;

        [SerializeField]
        OpenEventContainer m_OpenEvents;

        [SerializeField]
        CloseEventContainer m_CloseEvents;

        #endregion
        #region Properties

        public bool IsOpened
        {
            get { return m_Animator.GetBool(IS_OPENED); }
        }

        public OpenEventContainer OpenEvents
        {
            get { return m_OpenEvents; }
        }

        public CloseEventContainer CloseEvents
        {
            get { return m_CloseEvents; }
        }

        #endregion
        #region Methods

        void OnEnable()
        {
            OnValidate();

            if (m_OpenOnEnable)
                Open();
        }

        void OnValidate()
        {
            if (m_Animator == null)
                m_Animator = GetComponent<Animator>();
        }
        
        public void Open()
        {
            if (IsOpened)
                return;

            m_Animator.SetBool(IS_OPENED, true);
        }

        public void Close()
        {
            if (!IsOpened)
                return;

            m_Animator.SetBool(IS_OPENED, false);
        }

        #endregion
        #region Nested

        [System.Serializable]
        public struct OpenEventContainer
        {
            public UnityEvent OnStartOpen;
            public UnityEvent OnEndOpen;
        } 
        
        [System.Serializable]
        public struct CloseEventContainer
        {
            public UnityEvent OnStartClose;
            public UnityEvent OnEndClose;
        }

        #endregion
    }
}