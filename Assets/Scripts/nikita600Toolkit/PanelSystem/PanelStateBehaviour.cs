﻿using UnityEngine;

namespace nikita600Toolkit.PanelSystem
{
    public class PanelStateBehaviour : StateMachineBehaviour
    {
        #region Fields

        [SerializeField]
        bool m_UseOpenEvents;

        Panel m_Panel;

        #endregion
        #region Methods

        public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            if (!CachePanel(animator))
                return;

            if (m_UseOpenEvents)
            {
                m_Panel.OpenEvents.OnStartOpen.Invoke();
            }
            else
            {
                m_Panel.CloseEvents.OnStartClose.Invoke();
            }
        }

        public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            if (!CachePanel(animator))
                return;

            if (m_UseOpenEvents)
            {
                m_Panel.OpenEvents.OnEndOpen.Invoke();
            }
            else
            {
                m_Panel.CloseEvents.OnEndClose.Invoke();
            }
        }
        
        bool CachePanel(Animator _animator)
        {
            if (m_Panel == null)
                m_Panel = _animator.GetComponent<Panel>();

            return m_Panel != null;
        }

        #endregion
    }
}