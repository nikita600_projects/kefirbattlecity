﻿using KefirBattleCity.HealthSystem;
using UnityEngine;
using UnityEngine.Events;

namespace KefirBattleCity.GlueLogic
{
    [DisallowMultipleComponent]
    public class GameController : MonoBehaviour
    {
        #region Fields

        [SerializeField]
        WinCondition[] m_WinConditions;

        #endregion
        #region Methods

        void OnEnable()
        {
            HealthManager.OnHealthGroupEmpty += OnHealthGroupEmpty;
        }

        void OnDisable()
        {
            HealthManager.OnHealthGroupEmpty -= OnHealthGroupEmpty;
        }
        
        void OnHealthGroupEmpty(string _healthGroup)
        {
            CheckWinCondition(_healthGroup);
        }

        void CheckWinCondition(string _healthGroup)
        {
            var winConditionCount = m_WinConditions.Length;
            for (var i = 0; i < winConditionCount; ++i)
            {
                var condition = m_WinConditions[i];

                if (!condition.IsValid(_healthGroup))
                    continue;

                condition.InvokeOnValid.Invoke();
                break;
            }
        }

        #endregion
        #region Nested

        [System.Serializable]
        class WinCondition
        {
            #region Fields

            [SerializeField]
            string m_HealthGroup;

            [SerializeField]
            UnityEvent m_InvokeOnValid;

            #endregion
            #region Properties

            public UnityEvent InvokeOnValid
            {
                get { return m_InvokeOnValid; }
            }

            #endregion
            #region Methods

            public bool IsValid(string _healthGroup)
            {
                return string.Equals(m_HealthGroup, _healthGroup, System.StringComparison.Ordinal);
            }

            #endregion
        }

        #endregion
    }
}