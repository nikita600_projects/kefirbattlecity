﻿using KefirBattleCity.WeaponSystem;
using UnityEngine;

namespace KefirBattleCity.GlueLogic
{
    public class AIController : MonoBehaviour
    {
        #region Fields

        [SerializeField]
        string m_TargetTag = "Player";

        [SerializeField]
        float m_VisibleDistance = 5f;

        [SerializeField]
        Weapon[] m_Weapons;
        
        #endregion
        #region Methods

        void FixedUpdate()
        {
            var weaponCount = m_Weapons.Length;
            for (var i = 0; i < weaponCount; ++i)
            {
                var weapon = m_Weapons[i];
                var gunpoint = weapon.GunPoint;

                RaycastHit raycastHit;
                if (!Physics.Raycast(gunpoint.position, gunpoint.forward, out raycastHit, m_VisibleDistance))
                    continue;

                var collidedGameObject = raycastHit.collider.gameObject;
                if (collidedGameObject.tag != m_TargetTag)
                    continue;

                weapon.Shoot();
            }
        }

        #endregion
    }
}