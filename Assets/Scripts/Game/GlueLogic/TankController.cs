﻿using KefirBattleCity.InputSystem;
using KefirBattleCity.MovementSystem;
using KefirBattleCity.WeaponSystem;
using UnityEngine;

namespace KefirBattleCity.GlueLogic
{
    public class TankController : MonoBehaviour
    {
        #region Fields

        [SerializeField]
        TankRotations m_Rotations;

        [SerializeField]
        Movement m_MovementComponent;

        [SerializeField]
        Weapon m_Weapon;

        InputHandler m_InputHandler;

        #endregion
        #region Properties

        InputHandler InputHandler
        {
            get
            {
                if (m_InputHandler == null)
                    m_InputHandler = FindObjectOfType<InputHandler>();

                return m_InputHandler;
            }
        }

        #endregion
        #region Methods

        void FixedUpdate()
        {
            var inputHandler = InputHandler;
            if (inputHandler == null)
                return;

            var moveUp = inputHandler.UpButtonPressed;
            var moveDown = inputHandler.DownButtonPressed;
            var moveLeft = inputHandler.LeftButtonPressed;
            var moveRight = inputHandler.RightButtonPressed;
            
            var isMoving = moveUp || moveDown || moveLeft || moveRight;
            if (isMoving)
            {
                var direction = m_MovementComponent.FacingDirection.forward;
                m_MovementComponent.Move(direction);

                var rotation = transform.rotation;
                rotation = moveUp ? m_Rotations.Up : moveDown ? m_Rotations.Down : rotation;
                rotation = moveLeft ? m_Rotations.Left : moveRight ? m_Rotations.Right : rotation;
                m_MovementComponent.Rotate(rotation);
            }

            var isShooting = inputHandler.ShootButtonPressed;
            if (isShooting)
                m_Weapon.Shoot();
        }
        
        #endregion
        #region Nested

        [System.Serializable]
        struct TankRotations
        {
            #region Fields

            [SerializeField]
            Vector3 m_Up;

            [SerializeField]
            Vector3 m_Down;
            
            [SerializeField]
            Vector3 m_Left;

            [SerializeField]
            Vector3 m_Right;
            
            #endregion
            #region Properties

            public Quaternion Up
            {
                get { return Quaternion.Euler(m_Up); }
            }

            public Quaternion Down
            {
                get { return Quaternion.Euler(m_Down); }
            }

            public Quaternion Left
            {
                get { return Quaternion.Euler(m_Left); }
            }

            public Quaternion Right
            {
                get { return Quaternion.Euler(m_Right); }
            }

            #endregion
        }
        
        #endregion
    }
}