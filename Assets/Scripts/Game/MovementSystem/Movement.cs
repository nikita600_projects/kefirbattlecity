﻿using UnityEngine;

namespace KefirBattleCity.MovementSystem
{
    public abstract class Movement : MonoBehaviour
    {
        #region Fields

        [SerializeField]
        Transform m_FacingDirection;

        [SerializeField]
        protected float m_Speed;

        #endregion
        #region Properties

        public Transform FacingDirection
        {
            get { return m_FacingDirection; }
        }

        #endregion
        #region Methods

        public abstract void Move(Vector3 _direction);

        public abstract void Rotate(Quaternion _rotation);

        void OnEnable()
        {
            OnValidate();
        }

        protected virtual void OnValidate()
        {
            m_Speed = Mathf.Clamp(m_Speed, 1, int.MaxValue);
        }

        #endregion
    }
}