﻿using UnityEngine;

namespace KefirBattleCity.MovementSystem
{
    [RequireComponent(typeof(Rigidbody))]
    public class RigidbodyMovement : Movement
    {
        #region Fields

        [SerializeField]
        Rigidbody m_Rigidbody;

        #endregion
        #region Methods

        public override void Move(Vector3 _direction)
        {
            var positionOffset = _direction * m_Speed * Time.fixedDeltaTime;
            var nextPosition = m_Rigidbody.position + positionOffset;

            m_Rigidbody.MovePosition(nextPosition);
        }

        public override void Rotate(Quaternion _rotation)
        {
            m_Rigidbody.MoveRotation(_rotation);
        }

        protected override void OnValidate()
        {
            base.OnValidate();

            if (m_Rigidbody == null)
                m_Rigidbody = GetComponent<Rigidbody>();
        }

        #endregion
    }
}