﻿using nikita600Toolkit.ObjectPoolSystem;
using UnityEngine;

namespace KefirBattleCity.WeaponSystem
{
    public class Weapon : MonoBehaviour
    {
        #region Fields
        
        [SerializeField]
        SceneObjectPool m_ObjectPool;

        [SerializeField]
        Transform m_GunPoint;

        [SerializeField]
        float m_ReloadTime = 1f;

        [SerializeField]
        float m_Force;
        
        float m_LastShootTime;

        #endregion
        #region Properties

        public Transform GunPoint
        {
            get { return m_GunPoint; }
        }

        bool IsCanShoot
        {
            get
            {
                var deltaTile = Time.time - m_LastShootTime;
                return deltaTile > m_ReloadTime;
            }
        }

        #endregion
        #region Methods

        public void Shoot()
        {
            if (!IsCanShoot)
                return;

            var projectileObject = m_ObjectPool.Pop();
            var projectileTransform = projectileObject.transform;

            projectileTransform.position = m_GunPoint.position;
            projectileTransform.rotation = m_GunPoint.rotation;

            var projectile = projectileObject.GetComponent<Projectile>();
            projectile.Launch(m_GunPoint.forward * m_Force);

            m_LastShootTime = Time.time;
        }

        #endregion
    }
}