﻿using KefirBattleCity.HealthSystem;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace KefirBattleCity.WeaponSystem
{
    [RequireComponent(typeof(Rigidbody))]
    public class Projectile : MonoBehaviour
    {
        #region Fields

        [SerializeField]
        int m_DamagePoints = 1;

        [SerializeField]
        List<string> m_DamageableTags = new List<string>();

        [SerializeField]
        TargetObjectDamaged m_OnTargetObjectDamaged;

        [SerializeField]
        List<string> m_IgnoreCollisions = new List<string>(); // TODO: Использовать CollisionMatrix или нет?
        
        [SerializeField]
        CollisionEnter m_OnCollisionEnter;
        
        [SerializeField, HideInInspector]
        Rigidbody m_Rigidbody;
        
        #endregion
        #region Methods

        public void Launch(Vector3 _force)
        {
            m_Rigidbody.velocity = Vector3.zero;
            m_Rigidbody.AddForce(_force, ForceMode.Impulse);
        }
        
        void OnCollisionEnter(Collision collision)
        {
            var collidedGameObject = collision.gameObject;
            var collidedGameObjectTag = collidedGameObject.tag;

            if (m_DamageableTags.Contains(collidedGameObjectTag))
            {
                var health = collidedGameObject.GetComponent<Health>();
                if (health != null)
                {
                    health.TakeDamage(m_DamagePoints);
                    m_OnTargetObjectDamaged.Invoke();
                }
            }

            if (!m_IgnoreCollisions.Contains(collidedGameObjectTag))
                m_OnCollisionEnter.Invoke();
        }

        void OnValidate()
        {
            m_DamagePoints = Mathf.Clamp(m_DamagePoints, 1, int.MaxValue);

            if (m_Rigidbody == null)
                m_Rigidbody = GetComponent<Rigidbody>();
        }

        #endregion
        #region Nested

        [System.Serializable]
        class CollisionEnter : UnityEvent { }

        [System.Serializable]
        class TargetObjectDamaged : UnityEvent { }

        #endregion
    }
}