﻿using nikita600Toolkit.GridComponents;
using System;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace KefirBattleCity.LevelSystem
{
    [Serializable]
    public struct LevelChunkData : ISerializationCallbackReceiver
    {
        #region Fields

#if UNITY_EDITOR
        [SerializeField]
        GameObject m_prefab;
#endif

        [SerializeField]
        Vector2Int m_Position;
        
        [SerializeField]
        string m_AssetBundleName;

        [SerializeField]
        string m_AssetName;
        
        #endregion
        #region Properties

        public Vector2Int Position
        {
            get { return m_Position; }
        }

        public string AssetBundleName
        {
            get { return m_AssetBundleName; }
        }

        public string AssetName
        {
            get { return m_AssetName; }
        }

        #endregion
        #region Constructor



        #endregion
        #region ISerializationCallbackReceiver

        void ISerializationCallbackReceiver.OnBeforeSerialize()
        {
#if UNITY_EDITOR

            Debug.Log("ED_Call");

            if (m_prefab == null)
            {
                m_Position = Vector2Int.zero;
                m_AssetBundleName = string.Empty;
                m_AssetName = string.Empty;

                return;
            }

            var objectPath = AssetDatabase.GetAssetPath(m_prefab);

            if (string.IsNullOrEmpty(objectPath))
            {
                m_prefab = null;
                return;
            }

            var importer = AssetImporter.GetAtPath(objectPath);
            if (importer == null)
                return;

            m_AssetBundleName = importer.assetBundleName;
            m_AssetName = importer.assetPath;
#endif
        }

        void ISerializationCallbackReceiver.OnAfterDeserialize() { }

        #endregion
    }
}