﻿using UnityEngine;

namespace KefirBattleCity.InputSystem
{
    public abstract class InputHandler : MonoBehaviour
    {
        #region Properties

        // D-Pad
        public abstract bool UpButtonPressed { get; }
        public abstract bool DownButtonPressed { get; }
        public abstract bool LeftButtonPressed { get; }
        public abstract bool RightButtonPressed { get; }

        // Action
        public abstract bool ShootButtonPressed { get; }
        public abstract bool PauseButtonPressed { get; }

        #endregion
    }
}