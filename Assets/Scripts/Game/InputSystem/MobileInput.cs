﻿namespace KefirBattleCity.InputSystem
{
    public sealed class MobileInput : InputHandler
    {
        #region ExposedForEditorProperties

        public bool UpButton { get; set; }
        public bool DownButton { get; set; }
        public bool LeftButton { get; set; }
        public bool RightButton { get; set; }

        public bool ShootButton { get; set; }
        public bool PauseButton { get; set; }

        #endregion
        #region Properties

        public override bool UpButtonPressed
        {
            get { return UpButton; }
        }

        public override bool DownButtonPressed
        {
            get { return DownButton; }
        }

        public override bool LeftButtonPressed
        {
            get { return LeftButton; }
        }

        public override bool RightButtonPressed
        {
            get { return RightButton; }
        }

        public override bool ShootButtonPressed
        {
            get { return ShootButton; }
        }

        public override bool PauseButtonPressed
        {
            get { return PauseButton; }
        }

        #endregion
    }
}

