﻿using UnityEngine;

namespace KefirBattleCity.InputSystem
{
    public sealed class StandaloneInput : InputHandler
    {
        #region Fields

        [Header("D-Pad")]
        [SerializeField]
        KeyCode m_UpButtonKey = KeyCode.W;

        [SerializeField]
        KeyCode m_DownButtonKey = KeyCode.S;

        [SerializeField]
        KeyCode m_LeftButtonKey = KeyCode.A;

        [SerializeField]
        KeyCode m_RightButtonKey = KeyCode.D;

        [Header("Action Buttons")]
        [SerializeField]
        KeyCode m_ShootButtonKey = KeyCode.Space;

        [SerializeField]
        KeyCode m_PauseButtonKey = KeyCode.Escape;

        #endregion
        #region Properteis

        public override bool UpButtonPressed
        {
            get { return Input.GetKey(m_UpButtonKey); }
        }

        public override bool DownButtonPressed
        {
            get { return Input.GetKey(m_DownButtonKey); }
        }

        public override bool LeftButtonPressed
        {
            get { return Input.GetKey(m_LeftButtonKey); }
        }

        public override bool RightButtonPressed
        {
            get { return Input.GetKey(m_RightButtonKey); }
        }

        public override bool ShootButtonPressed
        {
            get { return Input.GetKeyDown(m_ShootButtonKey); }
        }

        public override bool PauseButtonPressed
        {
            get { return Input.GetKeyDown(m_PauseButtonKey); }
        }

        #endregion
    }
}