﻿using UnityEngine;
using System.Collections.Generic;

namespace KefirBattleCity.HealthSystem
{
    public sealed class HealthManager
    {
        #region Events
         
        public static event System.Action<string> OnHealthGroupEmpty; 

        #endregion
        #region Fields

        static readonly Dictionary<string, List<Health>> s_ActiveHealthGroups = new Dictionary<string, List<Health>>();

        #endregion
        #region Methods

        public static void Register(Health _health)
        {
            var healthGroup = _health.tag;

            if (!s_ActiveHealthGroups.ContainsKey(healthGroup))
            {
                var healthGroupList = new List<Health> { _health };
                s_ActiveHealthGroups.Add(healthGroup, healthGroupList);
            }
            else
            {
                var healthGroupList = s_ActiveHealthGroups[healthGroup];
                healthGroupList.Add(_health);
            }
        }

        public static void Unregister(Health _health)
        {
            var healthGroup = _health.tag;

            if (!s_ActiveHealthGroups.ContainsKey(healthGroup))
                return;

            var healthGroupList = s_ActiveHealthGroups[healthGroup];
            healthGroupList.Remove(_health);

            if (healthGroupList.Count == 0 && OnHealthGroupEmpty != null)
            {
                OnHealthGroupEmpty(healthGroup);
                Debug.Log(healthGroup);
            }
                
        }

        public static List<Health> GetHealthGroupList(string _groupTag)
        {
            return s_ActiveHealthGroups.ContainsKey(_groupTag) ? s_ActiveHealthGroups[_groupTag] : default(List<Health>);
        }

        #endregion
    }
}