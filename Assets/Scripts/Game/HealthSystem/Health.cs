﻿using UnityEngine;
using UnityEngine.Events;

namespace KefirBattleCity.HealthSystem
{
    public class Health : MonoBehaviour
    {
        #region Fields

        [SerializeField]
        int m_HealthPoints = 1;
        
        [SerializeField]
        float m_DamageCooldownTime = 1f;

        [SerializeField]
        HealthPointsEmpty m_OnHealthPointsEmpty;

        float m_LastDamageTime;

        #endregion
        #region Properties

        int HealthPoints
        {
            get { return m_HealthPoints; }
            set
            {
                m_HealthPoints = Mathf.Clamp(value, 0, int.MaxValue);

                if (m_HealthPoints == 0)
                    m_OnHealthPointsEmpty.Invoke();
            }
        }
        
        #endregion
        #region Methods

        public void TakeDamage(int _damagePoints)
        {
            var currentTime = Time.time;
            var damageCooldownDelta = currentTime - m_LastDamageTime;

            if (damageCooldownDelta < m_DamageCooldownTime)
                return;

            HealthPoints -= _damagePoints;
            m_LastDamageTime = currentTime;
        }

        void OnEnable()
        {
            m_LastDamageTime = default(float);

            HealthManager.Register(this);
        }

        void OnDisable()
        {
            m_LastDamageTime = default(float);

            HealthManager.Unregister(this);
        }

        void OnValidate()
        {
            HealthPoints = m_HealthPoints;
            m_DamageCooldownTime = Mathf.Clamp(m_DamageCooldownTime, 0.01f, float.MaxValue);
        }

        #endregion
        #region Events

        [System.Serializable]
        class HealthPointsEmpty : UnityEvent { }

        #endregion
    }
}