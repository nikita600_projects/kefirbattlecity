﻿using UnityEngine;
using System.Collections;
using System.IO;

using KefirBattleCity.LevelSystem;
using nikita600Toolkit.AssetSystem;

public class AssetLoader : MonoBehaviour
{
    [SerializeField]
    AssetMetadata m_AssetMetadata;

    void OnEnable()
    {
        StartCoroutine(LoadLevelChunk(m_AssetMetadata));
    }

    IEnumerator LoadLevelChunk(AssetMetadata _data)
    {
        var path = string.Concat("file://", Application.streamingAssetsPath, "/", _data.AssetBundleName);
        var www = WWW.LoadFromCacheOrDownload(path, 0);
        yield return www;

        var assetBundle = www.assetBundle;
        if (assetBundle == null)
        {
            Debug.Log("FUCK");
        }
        else
        {
            Debug.Log("SHIT");
            
            var asset = assetBundle.LoadAsset(_data.AssetPath) as GameObject;
            if (asset == null)
            {
                Debug.Log("YOB TVOY MAT");
            }
            else
            {
                Instantiate(asset);
            }
        }

        yield return null;
    }
}
